<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['tentang-kami'] = 'home/about_us';
$route['kontak-kami'] = 'home/contact_us';
$route['pertanyaan-umum'] = 'home/faq';
$route['syarat-dan-ketentuan'] = 'home/tos';
$route['kebijakan-privasi'] = 'home/pop';
$route['artikel/(:any)'] = 'artikel/index/$1';
$route['produk/(:any)/(:any)'] = 'produk/index/$1/$2';