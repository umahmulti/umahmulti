<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages']	= array();
$autoload['libraries']	= array('database', 'session', 'l_option', 'l_menu');
//$autoload['libraries'] = array();
$autoload['drivers']	= array();
$autoload['helper']		= array('url', 'file', 'form');
$autoload['config']		= array();
$autoload['language']	= array();
$autoload['model']		= array();
