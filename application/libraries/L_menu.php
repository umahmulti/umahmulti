<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class L_menu{
    private $ci;
    
    function __construct(){
        $this->ci =& get_instance();
    }

    function get_menu($id=null,$parent=null)
    {
        $db = $this->ci->db;

        $whr = array('is_active'=>'Y');
        if($id!=null) {
            $whr = array(
                        'menu_level'=>$id,
                        'menu_loc'=>'1',
                        'is_active'=>'Y'
                    );
        }
        if($parent!=null) {
            $whr = array(
                        'id_menu'=>$parent,
                        'menu_loc'=>'1',
                        'is_active'=>'Y'
                    );
        }

        $db->where($whr);
        $db->order_by('menu_order','ASC');
        $res = $db->get('umh_m9l2t0_menu');

        if($res->num_rows() > 0) {
            if($parent!=null) {
                return $res->row();
            }
            return $res;
        }
        return 0;
    }

    function get_submenu($id)
    {
        $db = $this->ci->db;

        $db->where(array('menu_parent'=>$id, 'menu_loc'=>'1', 'is_active'=>'Y'));
        $db->order_by('menu_order','ASC');
        $res = $db->get('umh_m9l2t0_menu');

        return $res;
    }
}