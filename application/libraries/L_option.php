<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class L_option{
    private $ci;
    
    function __construct(){
        $this->ci =& get_instance();
    }

    function encode($string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = 'siapasaya';
        $secret_iv = 'sayakamu';
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        $output = str_replace('=', '', $output);
        return $output;
    }

    function decode($string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = 'siapasaya';
        $secret_iv = 'sayakamu';
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        return $output;
    }

    function generate_unique_id($len=null)
    {
        $this->ci->load->helper('string');
        if($len==null){
            //param strin : alpha | alnum | numeric | nozero | unique | sha1
            //default length 8 char
            $unique_id = random_string('alnum');
        }
        else{
            $unique_id = random_string('alnum',$len);
        }
        return $unique_id;
    }

    function get_products($id=null, $cat=null, $limit=null)
    {
        $db = $this->ci->db;

        $whr = array('is_active'=>'Y');
        if($id!=null) {
            $whr = array(
                        'parent'=>$id,
                        'is_active'=>'Y'
                    );
        }
        if($cat!=null) {
            $whr = array(
                        'product_cat'=>$cat,
                        'is_active'=>'Y'
                    );
        }
        if($id!=null && $cat!=null) {
            $whr = array(
                        'parent'=>$id,
                        'product_cat'=>$cat,
                        'is_active'=>'Y'
                    );
        }

        $db->where($whr);
        $db->order_by('product_name','ASC');
        if($limit!=null) {
            if($limit==1) {
                $limit = 0;
            }
            $db->limit(3,$limit);
        }
        $res = $db->get('umh_m9l2t0_products');

        return $res;
    }

    function get_img($loc, $parent=null)
    {
        $db = $this->ci->db;

        $whr = array('img_loc'=>$loc,
                    'is_active'=>'Y'            
                    );
        if($parent!='') {
            $whr['parent'] = $parent;
        }
        $db->where($whr);
        $db->order_by('img_order', 'ASC');
        $res = $db->get('umh_m9l2t0_image');

        return $res;
    }

    function get_category()
    {
        $db = $this->ci->db;

        $db->where(array('is_active'=>'Y'));
        $db->order_by('cat_name','DESC');
        $res = $db->get('umh_m9l2t0_category');

        return $res;
    }

    function ins_url($url, $prior, $date = null)
    {
        $db = $this->ci->db;
        $chck = $this->chck_url($url);
        if($chck->num_rows()==0) {
            $save = array('id_sitemap' => $this->generate_unique_id(10),
                        'url'          => $url,
                        'prior'        => $prior,
                        'created_date' => $date
                        );
            $db->insert('umh_m9l2t0_sitemap', $save);
        }
    }

    function chck_url($url)
    {
        $db = $this->ci->db;
        $db->where(array('url'=>$url, 'is_active'=>'Y'));
        $res = $db->get('umh_m9l2t0_sitemap');
        return $res;
    }

    function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
}