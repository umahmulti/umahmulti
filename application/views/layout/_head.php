<link rel="shortcut icon" href="<?php echo config_item('umah'); ?>img/umahmulti_logo.png">
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="robots" content="index, follow" />
<meta name="google-site-verification" content="oDkzi3i_8XyllIaXSChs3cwBRI9lKjqSsdI-GIJPyKU" />
<meta name="googlebot" content="index, follow" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="keywords" content="<?php echo $meta_key; ?>" />
<meta name="description" content="<?php echo $meta_desc; ?>" />
<meta name="author" content="Umah Multi Admin">
<!-- Facebook Meta Tag -->
<meta property="og:type" content="website">
<meta property="og:title" content="<?php echo $title; ?>">
<meta property="og:description" content="<?php echo $meta_desc; ?>">
<meta property="og:image" content="<?php echo $meta_img; ?>">
<meta name="facebook-domain-verification" content="dmhsmlk651o4qatqdxzyce2622tz0w" />
<!-- Twitter Meta Tag -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="<?php echo $meta_url; ?>">
<meta property="twitter:title" content="<?php echo $title; ?>">
<meta property="twitter:description" content="<?php echo $meta_desc; ?>">
<meta property="twitter:image" content="<?php echo $meta_img; ?>">
<meta name="twitter:image:alt" content="<?php echo $meta_img_alt; ?>">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="<?php echo config_item('umah'); ?>css/bootstrap.min.css">
<!-- fontawesome -->
<link rel="stylesheet" href="<?php echo config_item('umah'); ?>css/font-awesome-4.7.0.min.css">
<!-- Custom Css -->
<link rel="stylesheet" href="<?php echo config_item('umah'); ?>css/style.css">
<link rel="stylesheet" href="<?php echo config_item('umah'); ?>css/bootstrap-select.min.css">
<!-- toast CSS -->
<link href="<?php echo config_item('umah'); ?>css/jquery.toast.css" rel="stylesheet">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-PVM11GPQ6H"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-PVM11GPQ6H');
</script>