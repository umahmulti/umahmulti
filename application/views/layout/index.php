<!doctype html>
<html lang="id">
  <head>
    <?php
      if(!isset($meta_tag)) {
        $meta_tag = array('meta_key'    => 'percetakan terdekat,usaha percetakan,percetakan buku,spanduk percetakan,bisnis percetakan,tempat percetakan terdekat,percetakan foto terdekat,percetakan buku yasin terdekat,percetakan banner terdekat,percetakan stiker terdekat,percetakan undangan terdekat,toko percetakan terdekat,percetakan al quran',
                        'meta_desc'     => 'Jasa percetakan banner,spanduk,buku,undangan dan lainnya flat ongkir JaDeTaBek Rp 10.000*.Kami juga menyediakan Jasa Desain Grafis dan Website.',
                        'meta_img'      => config_item('umah').'img/umahmulti-big-logo.png',
                        'meta_img_alt'  => 'Logo Umah Multi',
                        'meta_url'      => base_url()
                        );
      }
      if(isset($title)) {
        $exp_art = explode(' - ', $title);
        if($exp_art[0] == 'Artikel') {
          $meta_tag['title'] = $title;
    ?>
      <title><?php echo $title; ?> - Umah Multi</title>

    <?php
        } else {
          $meta_tag['title'] = $title.' - Umah Multi';
    ?>

      <title><?php echo $title; ?> - Umah Multi Jasa Percetakan Digital & Offset</title>
      
    <?php
        }
      } else {
        $meta_tag['title'] = 'Jasa Percetakan Digital, Desain Grafis & Website - Umah Multi';
    ?>

      <title>Jasa Percetakan Digital, Desain Grafis & Website - Umah Multi</title>

    <?php
    }
      $this->load->view('layout/_head', $meta_tag);

      if(isset($css)){ echo $css; };
    ?>
  </head>
  <body>
    <!-- Sticky Menu Sosial Media -->
    <?php $this->load->view('layout/_sosmed_contact'); ?>
    <!-- Sticky Menu Sosial Media -->
    <header>
      <!-- Navbar -->
      <section id="nav-bar-menu" class="nav-bar-menu">
        <nav class="navbar navbar-expand-lg navbar-light fixed-top">
          <?php $this->load->view('layout/_menu'); ?>
        </nav>
      </section>
      <!-- End Navbar -->
      <!-- Modal Track Order -->
      <?php //$this->load->view('layout/_lacak_order'); ?>
      <!-- End Modal Track Order -->
    </header>
    <!-- Main -->
    <main role="main" class="inner-cover">
      <?php if(isset($body)){ echo $body; }; ?>
      <svg class="d-none d-md-block" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path fill="#444d55" fill-opacity="1" d="M0,160L120,181.3C240,203,480,245,720,250.7C960,256,1200,224,1320,208L1440,192L1440,320L1320,320C1200,320,960,320,720,320C480,320,240,320,120,320L0,320Z"></path>
      </svg>
    </main>
    <!-- End Main -->
    <!-- Section Wigdet -->
    <section class="widget py-5">
      <?php $this->load->view('layout/_widget_footer'); ?>
    </section>
    <!-- Section Wigdet -->
    <!-- Footer -->
    <section class="footer">
      <nav class="bg-dark text-center">
      <!-- Copyright -->
        <div class="footer-copyright py-3">©
          <script>document.write(new Date().getFullYear())</script>
          <a href="/">Umah Multi</a>
        </div>
        <!-- End Copyright -->
      </nav>
    </section>
    <!-- End Footer -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- Main JS -->
    <?php $this->load->view('layout/_js'); ?>
    <!-- Additional JS -->
    <?php if(isset($js)){ echo $js; }; ?>
  </body>
</html>