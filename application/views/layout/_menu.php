<div class="container">
  <a href="<?php echo base_url(); ?>"><img src="<?php echo config_item('umah'); ?>img/umahmulti.png" alt="Logo Umah Multi" title="Logo Umah Multi" loading="lazy"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mobile_nav" aria-controls="mobile_nav" aria-expanded="false" aria-label="Toggle navigation">
   <span class="navbar-toggler-icon"></span> 
  </button>
  <div class="collapse navbar-collapse" id="mobile_nav">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0 float-md-right">
    </ul>
    <ul class="navbar-nav navbar-light">
      <?php
        $menu = $this->l_menu->get_menu('1');
        if($menu->num_rows() > 0) {
          foreach($menu->result() as $rowm) {
            if($rowm->id_menu != 'S0tPR2FjRk5wWjM1Y0h0UWxxNnNSRlp0eTlJdEw4REpGSVNCOUgvMFd1cz0') {
              $menuname = strtolower(str_replace(' ', '-', $rowm->menu_name));
      ?>
          <li class="nav-item dropdown megamenu-li dmenu">
            <a class="nav-link dropdown-toggle" href="" id="officeNeed" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $rowm->menu_name; ?></a>
            <div class="dropdown-menu megamenu sm-menu border-top" aria-labelledby="officeNeed">
            <div class="row">
          <?php
            $submenu = $this->l_menu->get_submenu($rowm->id_menu);
            if($submenu->num_rows() > 0) {
              foreach($submenu->result() as $rowsub) {
          ?>
            <div class="col-sm-6 col-lg-2 border-right mb-4">
              <h6><i class="fa <?php echo $rowsub->menu_icon; ?>" aria-hidden="true"></i> <?php echo $rowsub->menu_name; ?></h6>
              <?php
                $product = $this->l_option->get_products($rowsub->id_menu);
                if($product->num_rows() > 0) {
                  foreach($product->result() as $rowp) {
                    $productname = strtolower(str_replace(' ', '-', $rowp->product_name));
                    // $productname = strtolower(preg_replace('~[\\\\/:*?"<>|]~', '-', $rowp->product_name));
                    $urlProd = base_url().'produk/'.$menuname.'/'.$productname;
                    $date_sitemap = ($rowp->modified_date!=''?$rowp->modified_date:$rowp->created_date);
                    $this->l_option->ins_url($urlProd, 0.9, $date_sitemap);
              ?>
                <a class="dropdown-item" href="<?php echo $urlProd; ?>"><?php echo $rowp->product_name; ?></a>
              <?php
                  }
                }
              ?>
            </div>
          <?php
              }
            }
          ?>
                <div class="col-sm-12 col-lg-2 mb-4">
                  <img src="<?php echo config_item('umah'); ?>img/umahmulti.png" loading="lazy" alt="Logo Umah Multi" title="Logo Umah Multi">
                </div>
              </div>
            </div>
          </li>
      <?php
        } else {
      ?>
          <li class="nav-item dmenu dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="services" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php echo $rowm->menu_name; ?>
            </a>
            <div class="dropdown-menu sm-menu" aria-labelledby="services">
              <?php
                $servprod = $this->l_option->get_products($rowm->id_menu);
                if($servprod->num_rows() > 0) {
                  foreach($servprod->result() as $rowserv) {
                    $servname = strtolower(str_replace(' ', '-', $rowserv->product_name));
                    $urlJasa = base_url().'produk/jasa/'.$servname;
                    $date_sitemap = ($rowserv->modified_date!=''?$rowserv->modified_date:$rowserv->created_date);
                    $this->l_option->ins_url($urlJasa, 0.9, $date_sitemap);
              ?>
                <a class="dropdown-item" href="<?php echo $urlJasa; ?>"><?php echo $rowserv->product_name; ?></a>
              <?php
                  }
                }
              ?>
            </div>
          </li>
      <?php
            }
          }
        }
      ?>
    </ul>
  </div>
</div>