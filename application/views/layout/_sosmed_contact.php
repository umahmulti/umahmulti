<div class="container">
  <div class="sticky-container in-right">
    <ul class="sticky">
     <li>
      <img src="<?php echo config_item('umah'); ?>img/facebook-circle.png" loading="lazy" width="32" height="32" alt="Umah Multi">
      <p><a href="https://web.facebook.com/umahmulti.id" target="_blank">Sukai kami di<br>Facebook</a></p>
     </li>
     <li>
      <img src="<?php echo config_item('umah'); ?>img/twitter-circle.png" loading="lazy" width="32" height="32" alt="Umah Multi">
      <p><a href="https://twitter.com/UmahMulti" target="_blank">Ikuti kami di<br>Twitter</a></p>
     </li>  
     <li>
      <img src="<?php echo config_item('umah'); ?>img/linkedin-circle.png" loading="lazy" width="32" height="32" alt="Umah Multi">
      <p><a href="https://www.linkedin.com/company/umahmulti/" target="_blank">Ikuti kami di<br>Linkedin</a></p>
     </li>
     <li>
      <img src="<?php echo config_item('umah'); ?>img/instagram-circle.png" loading="lazy" width="32" height="32" alt="Umah Multi">
      <p><a href="https://www.instagram.com/umahmulti.id/" target="_blank">Ikuti kami di<br>Instagram</a></p>
     </li>
     <li>
      <img src="<?php echo config_item('umah'); ?>img/whatsapp-circle.png" loading="lazy" width="32" height="32" alt="Umah Multi">
      <p><a href="https://api.whatsapp.com/send?phone=6285155001928&text=Saya%20tertarik%20dengan%20produk%20Umah%20Multi!" target="_blank">Hubungi Kami di<br>Whatsapp</a></p>
     </li>
     <!-- <li>
      <img src="<?php echo config_item('umah'); ?>img/search-circle.png" loading="lazy" width="32" height="32" alt="Umah Multi">
      <p class="py-auto"><a href="#track-order">Lacak Pesanan Anda</a></p>
     </li> -->
    </ul>
  </div>
</div>