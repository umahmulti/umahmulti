<div class="container">
  <div class="row">
    <div class="col-md-4">
      <img src="<?php echo config_item('umah'); ?>img/umahmulti.png" loading="lazy" alt="Umah Multi">
      <p class="text-justify">Jasa cetak spanduk, stiker, banner, kartu nama dan lainnya flat ongkir Rp 10.000,-* untuk wilayah Jadetabek. Kami juga menyediakan Jasa Desain Grafis dan Website.</p>
      <p>
        <h5 class="pb-2">Bagikan</h5>
        <a href="https://www.facebook.com/sharer.php?u=<?php echo base_url(); ?>" target="_blank"><img src="<?php echo config_item('umah'); ?>img/facebook-circle.png" loading="lazy" alt="Umah Multi"></a>
        <a href="https://twitter.com/share?url=<?php echo base_url(); ?>" target="_blank"><img src="<?php echo config_item('umah'); ?>img/twitter-circle.png" loading="lazy" alt="Umah Multi"></a>
        <a href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo base_url(); ?>" target="_blank"><img src="<?php echo config_item('umah'); ?>img/linkedin-circle.png" loading="lazy" alt="Umah Multi"></a>
        <a href="whatsapp://send?text=Jasa cetak spanduk, stiker, banner, kartu nama dan lainnya flat ongkir Jadetabek Rp 10.000. Kami juga menyediakan Jasa Desain Grafis dan Website. https://umahmulti.biz.id/" data-action="share/whatsapp/share" target="_blank"><img src="<?php echo config_item('umah'); ?>img/whatsapp-circle.png" loading="lazy" alt="Umah Multi"></a>
      </p>
    </div>
    <div class="col-md-4">
      <h5>Link Populer</h5>
      <ul class="list-unstyled text-small border-top">
        <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
        <li><a href="<?php echo base_url('tentang-kami'); ?>">Tentang Kami</a></li>
        <li><a href="<?php echo base_url('kontak-kami'); ?>">Kontak Kami</a></li>
        <li><a href="<?php echo base_url('artikel'); ?>">Artikel</a></li>
        <li><a href="<?php echo base_url('pertanyaan-umum'); ?>">Pertanyaan Umum</a></li>
        <li><a href="<?php echo base_url('syarat-dan-ketentuan'); ?>">Syarat & Ketentuan</a></li>
        <li><a href="<?php echo base_url('kebijakan-privasi'); ?>">Kebijakan Privasi</a></li>
      </ul>
    </div>
    <div id="track-order" class="col-md-4">
      <h5>Dukungan</h5>
      <ul class="list-unstyled text-small border-top">
        <li>
          <i class="fa fa-clock-o" aria-hidden="true"> Senin-Sabtu : 09.00 - 18.00</i>
        </li>
        <li>
          <i class="fa fa-clock-o" aria-hidden="true"> Minggu : 09.00 - 15.00</i>
        </li>
        <li>
          <i class="fa fa-whatsapp" aria-hidden="true"><a href="https://api.whatsapp.com/send?phone=6285155001928&text=Saya%20tertarik%20dengan%20produk%20Umah%20Multi!"> 085155001928</a></i>
        </li>
        <li><i class="fa fa-envelope" aria-hidden="true"><a href="mailto:info@umahmulti.biz.id"> info@umahmulti.biz.id</a></i>
        </li>
      </ul>
    </div>
  </div>
</div>