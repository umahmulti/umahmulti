<div class="modal fade" id="modalTrack" tabindex="-1" aria-labelledby="ModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="ModalLabel">Proses Order Anda</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Order ID :
      </div>
      <div class="container py-3">
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">Nama</th>
              <th scope="col">Alamat</th>
              <th scope="col">No. Telp</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Mark</th>
              <td>Otto</td>
              <td>@mdo</td>
            </tr>
          </tbody>
        </table>
        <div class="row text-center py-2 font-weight-bold">
          <div class="col">Verify Data</div>
          <div class="col">Cetak</div>
          <div class="col">Pengiriman</div>
          <div class="col">Telah Terkirim</div>
        </div>
        <div class="progress">
          <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 50%"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>