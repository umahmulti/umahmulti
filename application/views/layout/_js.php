<script src="<?php echo config_item('umah'); ?>js/jquery-3.5.1.min.js"></script>
<script src="<?php echo config_item('umah'); ?>js/popper.min.js"></script>
<script src="<?php echo config_item('umah'); ?>js/bootstrap.min.js"></script>
<script src="<?php echo config_item('umah'); ?>js/cust.js"></script>
<script src="<?php echo config_item('umah'); ?>js/bootstrap-select.min.js"></script>
<!-- toast -->
<script src="<?php echo config_item('umah'); ?>js/jquery.toast.js"></script>
<script>
	$(function() {
	  $('.selectpicker').selectpicker();
	});

	<?php if($this->session->flashdata('info')) : ?>
		$.toast({
			heading: 'Information !',
			text: '<?php echo $this->session->flashdata('info'); ?>' ,
			position: 'top-right',
			loaderBg: '#ff6849',
			icon: 'info',
			hideAfter: 6000,
			stack: 6
		});
	<?php endif; ?>
	
	<?php if($this->session->flashdata('warning')) : ?>
		$.toast({
			heading: 'Warning !',
			text: '<?php echo $this->session->flashdata('warning'); ?>',
			position: 'top-right',
			loaderBg: '#c29648',
			icon: 'warning',
			hideAfter: 6500,
			stack: 6
		});
	<?php endif; ?>
	
	<?php if($this->session->flashdata('success')) : ?>
		$.toast({
			heading: 'Success !',
			text: '<?php echo $this->session->flashdata('success'); ?>',
			position: 'top-right',
			loaderBg: '#ff6849',
			icon: 'success',
			hideAfter: 6500,
			stack: 6
		});
	<?php endif; ?>
	
	<?php if($this->session->flashdata('error')) : ?>
		$.toast({
			heading: 'Alert !',
			text: '<?php echo $this->session->flashdata('error'); ?>',
			position: 'top-right',
			loaderBg: '#ff6849',
			icon: 'error',
			hideAfter: 6500,
			stack: 6
		});
	<?php endif; ?>
</script>