<!-- Section Content -->
  <section id="desGraf" class="desGraf py-5">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-sm-12">
          <h1 class="display-4">Jasa Desain Grafis <b>(In Progress Guys !)</b></h1>
          <p class="lead">Mohon isi detail permintaan anda dengan jelas dan pastikan sudah benar.</p>
        </div>
        <div class="col-lg-4 col-sm-12 text-center my-auto">
          <img src="<?php echo config_item('umah'); ?>img/umahmulti.png" loading="lazy" alt="Logo Umah Multi" title="Logo Umah Multi">
        </div>
      </div>
    </div>
    <div class="container py-5 in-down border-top">
      <form>
        <div class="form-row pb-3">
          <div class="col">
            <input type="text" class="form-control" name="namePerson" placeholder="Nama">
          </div>
        </div>
        <div class="form-row py-3">
          <div class="col">
            <input type="email" class="form-control" name="emailAdd" placeholder="name@example.com">
          </div>
          <div class="col">
            <input type="text" class="form-control" name="noTelp" placeholder="No. Telp">
          </div>
        </div>
        <div class="form-group">
          <label for="desainOpt">Pilih Jenis Desain</label>
          <select class="form-control" id="desainOpt">
            <option>Pilih</option>
	    <option>Logo</option>
            <option>Media Cetak</option>
            <option>Abstrak</option>
            <option>Illustrasi</option>
            <option>Produk</option>
	    <option>Lainnya</option>
          </select>
        </div>
        <div class="form-group">
          <textarea class="form-control" name="msgContent" rows="3" placeholder="Isi Deskripsi Desain yang Anda Inginkan dengan Jelas & Detail"></textarea>
        </div>
        <a href=""><button type="button" class="btn btn-primary btn-lg float-right">Submit</button></a>
      </form>
    </div>
  </section>
<!-- End Section Content -->