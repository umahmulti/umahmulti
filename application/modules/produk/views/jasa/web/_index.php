<!-- Section Content -->
  <section id="compWeb" class="compWeb py-5">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-sm-12">
          <h1 class="display-4">Jasa Web Company Profile <b>(In Progress Guys !)</b></h1>
          <p class="lead">Buat Website Anda dengan Mudah dan Cepat!</p>
        </div>
        <div class="col-lg-4 col-sm-12 text-center my-auto">
          <img src="<?php echo config_item('umah'); ?>img/umahmulti.png" loading="lazy" alt="Logo Umah Multi" title="Logo Umah Multi">
        </div>
      </div>
    </div>
    <div class="container py-5 in-down border-top">
      <h1 class="text-center pb-2">Cek Domain Dulu Nih !</h1>
      <form>
        <div class="row justify-content-center">
          <div class="col-md-4">
            <div class="form-group">
              <input class="form-control form-control-lg" type="text" placeholder="Masukkan nama domain">
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <select class="form-control form-control-lg">
		<option>Pilih</option>
                <option>.com</option>
                <option>.my.id</option>
                <option>.biz.id</option>
              </select>
            </div>
          </div>
          <div class="col-md-2">
            <a href="#"><button type="button" class="btn btn-warning btn-lg">Cek</button></a>
          </div>
        </div>
      </form>
    <blockquote class="blockquote text-center my-2">
      <p></p> <!-- notif domain tersedia -->
    </blockquote>
    </div>
  </section>
<!-- End Section Content -->