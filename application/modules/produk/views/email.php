<!DOCTYPE html>
<html lang="id">
<head>
  <title>Permintaan Penawaran Harga - <?php echo $produk; ?></title>
  <!-- Css -->
  <style>
    h3{
      color: #4c628d;
    }
    table{
      border: 1px solid black;
      width: 60%;
      margin:auto;
    }
    td{
      padding: 10px;
    }
    .td-size {
      width: 100px;
    }
    .footer{
      text-align: center;
      color: #fff;
      background-color: #000
    }
    .footer td{
      border: 1px solid black;
    }
    .footer a{
      text-decoration: none;
      color: #fff;
    }
  </style>
</head>
<body>
  <table>
    <tr>
      <th colspan="3"><h3>Halo Admin</h3></th>
    </tr>
    <tr>
      <td colspan="3"><h3>Ada Pesanan Nih Dari - <?php echo $namaCust; ?></h3></td>
    </tr>
    <tr>
      <td class="td-size-1">Nomor Telp</td>
      <td> : </td>
      <td><?php echo $telpCust; ?></td>
    </tr>
    <tr>
      <td class="td-size-1">Email</td>
      <td> : </td>
      <td><?php echo $emailCust; ?></td>
    </tr>
    <tr>
      <td class="td-size">Alamat</td>
      <td> : </td>
      <td><?php echo $alamatCust; ?></td>
    </tr>
    <tr>
      <td class="td-size">Desain</td>
      <td> : </td>
      <td><?php echo ($isDesign=='1'?"Ada Desain":"Tidak Ada Desain"); ?></td>
    </tr>
    <tr>
      <td class="td-size">Qty</td>
      <td> : </td>
      <td><?php echo $quant[1]; ?></td>
    </tr>
    <tr>
      <td class="td-size">Catatan</td>
      <td> : </td>
      <td><?php echo $usrNotes; ?></td>
    </tr>
    <tr>
      <td colspan="3"><h3>Detail Pesanan Sebagai Berikut :</h3></td>
    </tr>
    <?php
      if($dtl_prod->num_rows()>0) {
        foreach($dtl_prod->result() as $row_dtl) {
    ?>
      <tr>
        <td class="td-size"><?php echo $row_dtl->field_name; ?></td>
        <td>:</td>
        <td><?php echo ${'dtl_'.$row_dtl->id_custom_field}; ?></td>
      </tr>
    <?php
        }
      }
    ?>
  </table>
  <table>
    <tr>
      <td class="footer">
        &#169;2021 <a href="https://umahmulti.biz.id/">Umah Multi</a>
      </td>
    </tr>
  </table>
</body>
</html>