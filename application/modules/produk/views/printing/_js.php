<script>
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

	$('#prodForm').submit(function() {
		if(confirm('Apakah Anda sudah yakin melakukan transaksi tersebut?')) {
			if(($('#namaCust').val() == '') && ($('#telpCust').val() == '') && ($('#emailCust').val() == '')) {
				$.toast({
					heading: 'Alert !',
					text: 'Mohon untuk mengisi Nama, Nomer dan Email Anda terlebih dahulu !' ,
					position: 'top-right',
					loaderBg: '#ff6849',
					icon: 'error',
					hideAfter: 6500,
					stack: 6
				});
			} else {
				$.ajax({
					type: 'POST',
					url: '<?php echo site_url('produk/save_tf'); ?>',
					datatype: 'json',
					data: $(this).serialize(),
					success: function(data) {
						jsonData = $.parseJSON(data);
						if(jsonData.status == 0) {
							$.toast({
								heading: 'Alert !',
								text: jsonData.error ,
								position: 'top-right',
								loaderBg: '#ff6849',
								icon: 'error',
								hideAfter: 6500,
								stack: 6
							});
						} else {
							$.toast({
								heading: 'Success !',
								text: jsonData.success ,
								position: 'top-right',
								loaderBg: '#ff6849',
								icon: 'success',
								hideAfter: 6500,
								stack: 6
							});

							location.reload();
						}
					}
				});
			}
		}
		return false;
	});
</script>