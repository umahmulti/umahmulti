<!-- Section Content -->
<?php $title = $this->l_option->decode($produk->id_product); ?>
<section id="sticker" class="sticker">
  <div class="container py-5">
    <div class="row">
      <div class="col-lg-8 col-sm-12">
        <h1 class="display-4">Cetak <?php echo $produk->product_name; ?></h1>
        <p class="lead">Mohon isi detail permintaan anda dengan jelas dan pastikan sudah benar.</p>
      </div>
    </div>
  </div>
  <div class="container py-5 border-top">
    <div class="row">
      <div class="col-lg-6 col-sm-12 text-center">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div id="custCarousel" class="carousel slide" data-ride="carousel" align="center">
                <!-- slides -->
                <div class="carousel-inner">
                  <?php
                    if($image->num_rows() > 0) {
                      $actv_img = "active";
                      foreach($image->result() as $row_img) {
                  ?>
                    <div class="carousel-item <?php echo $actv_img; ?>">
                      <img src="<?php echo base_url()."upload/img/produk/".$row_img->img_name; ?>" alt="<?php echo $row_img->img_alt; ?>" title="<?php echo $row_img->img_alt; ?>" loading="lazy">
                    </div>
                  <?php
                        $actv_img = '';
                      }
                    }
                  ?>
                </div>
                <!-- Indicator -->
                <a class="carousel-control-prev" href="#custCarousel" data-slide="prev">
                  <i class="fa fa-chevron-left" aria-hidden="true"></i>
                </a>
                <a class="carousel-control-next" href="#custCarousel" data-slide="next">
                  <i class="fa fa-chevron-right" aria-hidden="true"></i>
                </a>
                <!-- Thumbnails -->
                <ol class="carousel-indicators list-inline">
                  <?php
                    if($image->num_rows() > 0) {
                      $actv_img_sel = "active";
                      $no_img_sel = 0;
                      foreach($image->result() as $row_img_sel) {
                  ?>
                    <li class="list-inline-item <?php echo $actv_img_sel; ?>">
                        <a id="carousel-selector-<?php echo $no_img_sel; ?>" class="selected" data-slide-to="<?php echo $no_img_sel; ?>" data-target="#custCarousel">
                          <img src="<?php echo base_url()."upload/img/produk/".$row_img_sel->img_name; ?>" alt="<?php echo $row_img_sel->img_alt; ?>" title="<?php echo $row_img_sel->img_alt; ?>" loading="lazy">
                        </a>
                      </li>
                  <?php
                        $actv_img_sel = '';
                        $no_img_sel++;
                      }
                    }
                  ?>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-sm-12">
        <form id="prodForm" class="form-element" method="POST">
          <input type="hidden" name="produk" value="<?php echo $produk->m9l2t0_product_id; ?>">
          <input type="hidden" name="produkName" value="<?php echo $title; ?>">
          <div class="form-row">
            <div class="col">
              <label for="namaCust">Nama Anda</label>
              <input type="text" class="form-control" id="namaCust" name="namaCust" required>
            </div>
          </div>
          <div class="form-row mt-4">
            <div class="col">
              <label for="telpCust">Nomer Anda</label>
              <input type="text" class="form-control" id="telpCust" name="telpCust" required>
            </div>
          </div>
          <div class="form-row mt-4">
            <div class="col">
              <label for="emailCust">Email Anda</label>
              <input type="email" class="form-control" id="emailCust" name="emailCust" required>
            </div>
          </div>
          <div class="form-row mt-4">
            <div class="col">
              <label for="alamatCust">Alamat Anda</label>
              <textarea class="form-control" id="alamatCust" name="alamatCust" rows="3" required></textarea>
            </div>
          </div>
          <div class="form-row mt-4 border-top">
            <?php
              $dtl_prod = $this->produk_m->get_dtl_prod($produk->m9l2t0_product_id);
              if($dtl_prod->num_rows() > 0) {
                foreach($dtl_prod->result() as $row_dtl_prod) {
                  if($row_dtl_prod->type == 'dropdown') {
                    $list = explode(',',$row_dtl_prod->value);
            ?>
              <div class="form-group col-md-4">
                <label><?php echo $row_dtl_prod->field_name; ?></label>
                <select name="dtl_<?php echo $row_dtl_prod->id_custom_field; ?>" class="form-control selectpicker" data-live-search="true" required>
                  <option value="">-- Pilih --</option>
                  <?php
                    foreach($list as $opt) {
                  ?>
                    <option value="<?php echo $opt; ?>"><?php echo $opt; ?></option>
                  <?php } ?>
                </select>
              </div>
            <?php
                } elseif($row_dtl_prod->type == 'text') {
            ?>
              <div class="form-group col-md-4">
                <label><?php echo $row_dtl_prod->field_name; ?></label>
                <input type="text" class="form-control" name="dtl_<?php echo $row_dtl_prod->id_custom_field; ?>" placeholder="<?php echo $row_dtl_prod->placeholder; ?>" required>
              </div>
            <?php
                  }
                }
              }
            ?>
          </div>
          <!-- Cek Desain -->
          <div class="form-row py-3 border-top">
            <div class="col">
              <div class="form-check">
                <input class="form-check-input" type="radio" name="isDesign" id="yesCheck" value="1" required>
                <label class="form-check-label" for="yesCheck">
                <h6>Saya Sudah Punya Desain</h6>
                </label>
              </div>
            </div>
            <div class="col">
              <div class="form-check">
                <input class="form-check-input" type="radio" name="isDesign" id="noCheck" value="2">
                <label class="form-check-label" for="noCheck">
                <h6>Saya Belum Punya Desain</h6>
                </label>
              </div>
            </div>
          </div>
          <!-- End Cek Desain -->
          <div class="form-row py-3 ml-1">
            <div id="Check1" class="desc">
              <div class="form-group">
                <label>Masukkan Link File Penyimpanan Cloud (Google Drive, Dropbox, Dll.)</label>
                <textarea class="form-control" name="link_design" rows="1"></textarea>
              </div>
            </div>
            <div id="Check2" class="desc">
              <h4>Biaya Desain tergantung tingkat kesulitannya !</h4>
              <p>Untuk Desain kami akan menghubungi anda melalui Whatsapp !</p>
            </div>
          </div>
          <div class="form-row border-top"></div>
          <div class="form-group py-3">
            <label for="usrNotes">Catatan : Mohon isi dengan detail jika ada catatan.</label>
            <textarea class="form-control" id="usrNotes" name="usrNotes" rows="3"></textarea>
          </div>
          <div class="form-row py-3 border-top">
            <div class="col-lg-6 col-sm-4 pb-3">
              <div class="input-group">
                <h4>QTY :</h4>
                <span class="input-group-btn">
                  <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                      <i class="fa fa-minus" aria-hidden="true"></i>
                  </button>
                </span>
                <input type="text" name="quant[1]" class="form-control input-number" value="1" min="1" max="5000">
                <span class="input-group-btn">
                  <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
                      <i class="fa fa-plus" aria-hidden="true"></i>
                  </button>
                </span>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-primary btn-block">Kirim</button>
        </form>
      </div>
    </div>
  </div>
  <div class="container py-3 border-top">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item" role="presentation">
        <a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">Keterangan</a>
      </li>
    </ul>
    <div class="tab-content my-3" id="myTabContent">
      <div class="tab-pane fade show active text-justify" id="description" role="tabpanel" aria-labelledby="description-tab">
        <?php echo $produk->product_desc; ?>
      </div>
    </div>
    <!-- <a href="<?php echo base_url().'artikel/'.$title; ?>"><button type="button" class="btn btn-info my-3">Pelajari Lebih Lanjut</button></a> -->
  </div>
</section>
<!-- End Section Content