<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk_m extends CI_Model {

	function get_products($id)
	{
		$this->db->where(array('id_product'=>$id,'is_active'=>'Y'));
		$res = $this->db->get('umh_m9l2t0_products');
		return $res->row();
	}

	function get_dtl_prod($prod)
	{
		$sql = "SELECT * FROM umh_m9l2t0_custom_field WHERE FIND_IN_SET($prod, m9l2t0_product_id)  AND is_active = 'Y' ORDER BY field_order ASC";
		return $this->db->query($sql);
	}

	function save_tf($saved)
	{
		$res = $this->db->insert('umh_m9l2t0_transaksi', $saved);
		return $res;
	}

	function save_cust($saved)
	{
		$res = $this->db->insert('umh_m9l2t0_customer', $saved);
		return $res;
	}

	function save_dtl($saved)
	{
		$res = $this->db->insert('umh_m9l2t0_transaksi_detail', $saved);
		return $res;
	}

	function save_dtl_prod($saved)
	{
		$res = $this->db->insert('umh_m9l2t0_tr_detail_prod', $saved);
		return $res;
	}

	function check_cust($post)
	{
		$this->db->where(array('cust_firstname'=>$post['namaCust'],
							'cust_email'=>$post['emailCust'],
							'cust_phone'=>$post['telpCust'],
							'is_active'=>'Y'
						));
		$res = $this->db->get('umh_m9l2t0_customer');
		return $res;
	}
}

/* End of file Produk_m.php */
/* Location: ./application/models/Produk_m.php */