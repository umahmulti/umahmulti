<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function __construct(){
		parent::__construct();
	    $this->load->model('produk_m');
	}

	public function index($cat=null, $prod=null)
	{
		$title = $prod;
		if(preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $prod)) {
			$ex_title = explode('-', $prod);
			if($ex_title[1] == 'umkm') {
				$title = ucwords($ex_title[0])." ".strtoupper($ex_title[1]);
			} else {
				$title = ucwords(str_replace('-', ' ', $prod));
			}
		}

		if($cat!='jasa') {
			$id_prod = $this->l_option->encode($prod);
			$content['produk'] = $this->produk_m->get_products($id_prod);
			$ai_prod = $content['produk']->m9l2t0_product_id;
			$content['image'] = $this->l_option->get_img(2, $ai_prod);
			$data['title'] = "Cetak ".$title;

			// $data['css'] = $this->load->view('printing/_css', $content, TRUE);
			$data['js'] = $this->load->view('printing/_js', '', TRUE);
			$data['body'] = $this->load->view('printing/_index', $content, TRUE);
			$data['meta_tag'] = array('meta_key'    => $content['produk']->product_meta_keyword,
			                        'meta_desc'     => $content['produk']->product_desc_meta,
			                        'meta_img'      => base_url().'upload/img/produk/'.$content['produk']->product_img,
			                        'meta_img_alt'  => $content['produk']->product_img_alt,
						'meta_url'	=> base_url()
			                        );
		} else {
			$id_jasa = $this->l_option->encode($prod);
			$jasa = $this->produk_m->get_products($id_jasa);

			if($prod=='desain-grafis') {
				$data['body'] = $this->load->view('jasa/grafis/_index', '', TRUE);
			} else {
				$data['body'] = $this->load->view('jasa/web/_index', '', TRUE);
			}
			$data['title'] = "Jasa ".$title;
			$data['meta_tag'] = array('meta_key'    => $jasa->product_meta_keyword,
			                        'meta_desc'     => $jasa->product_desc_meta,
			                        'meta_img'      => base_url().'upload/img/produk/'.$jasa->product_img,
			                        'meta_img_alt'  => $jasa->product_img_alt,
						'meta_url'	=> base_url()
			                        );
		}

		$this->load->view('layout/index', $data);
	}

	function save_tf()
	{
		$post = $this->input->post();
		$this->load->config('email');
        $this->load->library('email');
        $cust_id = $this->l_option->generate_unique_id(10);
        $chck_cust = $this->produk_m->check_cust($post);
        if($chck_cust->num_rows()>0) {
        	$cust_id = $chck_cust->row()->id_cust;
        }
        $id_prod = $this->l_option->generate_unique_id(10);
        $saved = array('id_transaksi' 	=> $id_prod,
        			'cust_id'		  	=> $cust_id,
        			'is_design'			=> $post['isDesign'],
        			'link_design'		=> $post['link_design'],
        			'note_cust'			=> $post['usrNotes'],
        			'created_date'		=> date('Y-m-d H:i:s'),
					'created_by'		=> $this->session->userdata('id_users'),
					'created_ip'		=> $_SERVER['REMOTE_ADDR']
        			);
        $save_prod = $this->produk_m->save_tf($saved);
        if($save_prod) {
        	$id_tr_dtl = $this->l_option->generate_unique_id(10);
        	$saved_dtl_tr = array('id_tr_detail'=> $id_tr_dtl,
        					'id_transaksi'		=> $id_prod,
        					'product_id'		=> $post['produk'],
        					'qty'				=> $post['quant'][1],
        					'created_date'		=> date('Y-m-d H:i:s'),
							'created_by'		=> $this->session->userdata('id_users'),
							'created_ip'		=> $_SERVER['REMOTE_ADDR']
        					);
        	$save_tr_dtl = $this->produk_m->save_dtl($saved_dtl_tr);
        	if($save_tr_dtl) {
	        	$dtl_prod = $this->produk_m->get_dtl_prod($post['produk']);
	        	if($dtl_prod->num_rows() > 0) {
	        		foreach($dtl_prod->result() as $row_dtl) {
	        			$data_dtl_prod = array('id_detail_prod'	=> $this->l_option->generate_unique_id(10),
	        								'id_tr_detail'		=> $id_tr_dtl,
	        								'id_custom_field'	=> $row_dtl->id_custom_field,
	        								'value'				=> $post['dtl_'.$row_dtl->id_custom_field],
	        								'created_date'		=> date('Y-m-d H:i:s'),
											'created_by'		=> $this->session->userdata('id_users'),
											'created_ip'		=> $_SERVER['REMOTE_ADDR']
	        								);
	        			$this->produk_m->save_dtl_prod($data_dtl_prod);
	        		}
	        	}
	        }
	        if($chck_cust->num_rows()==0) {
	        	$saved_cust = array('id_cust'		=> $cust_id,
	        					'cust_firstname'	=> $post['namaCust'],
	        					'cust_email'		=> $post['emailCust'],
	        					'cust_phone'		=> $post['telpCust'],
	        					'cust_address'		=> $post['alamatCust'],
	        					'created_date'		=> date('Y-m-d H:i:s'),
								'created_by'		=> $this->session->userdata('id_users'),
								'created_ip'		=> $_SERVER['REMOTE_ADDR']
	        					);
	        	$this->produk_m->save_cust($saved_cust);
        	}
	        $produk = ucwords(str_replace('-', ' ', $post['produkName']));
	        
	        $from = $this->config->item('smtp_user');
	        $to = "umahmulti@gmail.com";
	        $subject = "Permintaan Penawaran Harga - $produk";
	        $message = $this->input->post('message');
	        $post['dtl_prod'] = $dtl_prod;

	        $this->email->from($from);
	        $this->email->to($to);
	        $this->email->subject($subject);
	        $this->email->message($this->load->view('email', $post, true));

	        if ($this->email->send()) {
	            $this->session->set_flashdata('success', 'Terima Kasih ! Permintaan Anda sudah terkirim, Kami segera menghubungi Anda melalui Email atau Whatsapp');
				$json = array('status'=> 1, 'success' => 'Terima Kasih ! Permintaan Anda sudah terkirim, Kami segera menghubungi Anda melalui Email atau Whatsapp');
	        } else {
	            $this->session->set_flashdata('error', 'Mohon Maaf permintaan Anda tidak bisa diproses, Mohon hubungi kami melalui Email atau Whatsapp');
	            $json = array('status'=> 0, 'error' => 'Mohon Maaf permintaan Anda tidak bisa diproses, Mohon hubungi kami melalui Email atau Whatsapp');
	        }
	    } else {
	    	$this->session->set_flashdata('error', 'Mohon Maaf permintaan Anda tidak bisa diproses, Mohon hubungi kami melalui Email atau Whatsapp');
	    	$json = array('status'=> 0, 'error' => 'Mohon Maaf permintaan Anda tidak bisa diproses, Mohon hubungi kami melalui Email atau Whatsapp');
	    }

	    echo json_encode($json);
	}


}

/* End of file Produk.php */
/* Location: ./application/controllers/Produk.php */