<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq_m extends CI_Model {

    function get_faq()
    {
    	$this->db->where(array('is_active'=>'Y'));
    	$this->db->order_by('faq_order', 'ASC');
    	$res = $this->db->get('umh_m9l2t0_faq');
    	return $res;
    }
}

/* End of file faq_m.php */
/* Location: ./application/models/faq_m.php */