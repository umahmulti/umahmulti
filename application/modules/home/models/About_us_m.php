<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About_us_m extends CI_Model {

    function get_about($loc)
    {
    	$this->db->where(array('loc'=>$loc, 'is_active'=>'Y'));
    	$res = $this->db->get('umh_m9l2t0_about');
    	return $res->row();
    }

    function get_our_team()
    {
    	$this->db->where(array('is_active'=>'Y'));
    	$this->db->order_by('ot_name', 'ASC');
    	$res = $this->db->get('umh_m9l2t0_our_team');
    	return $res;
    }
}

/* End of file about_us_m.php */
/* Location: ./application/models/about_us_m.php */