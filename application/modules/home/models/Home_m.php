<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_m extends CI_Model {

	function count_products($cat=null)
    {
        $whr = array('is_active'=>'Y');
        if($cat!=null) {
            $whr = array(
                        'product_cat'=>$cat,
                        'is_active'=>'Y'
                    );
        }

        $this->db->where($whr);
        $res = $this->db->get('umh_m9l2t0_products');

        return $res->num_rows();
    }

    function get_parent($id)
    {
        $this->db->select('menu_parent');
        $this->db->where(array('id_menu'=>$id, 'is_active'=>'Y'));
        $res = $this->db->get('umh_m9l2t0_menu');

        if($res->num_rows() > 0) {
            return $res->row();
        }
        return 0;
    }

    function get_service()
    {
    	$this->db->where(array('is_active'=>'Y'));
    	$res = $this->db->get('umh_m9l2t0_services');
    	return $res;
    }

    function get_cust_review()
    {
    	$this->db->where(array('is_active'=>'Y'));
    	$this->db->order_by('created_date', 'DESC');
    	$this->db->limit(6);
    	$res = $this->db->get('umh_m9l2t0_cust_review');
    	return $res;
    }

    function get_our_team()
    {
    	$this->db->where(array('is_active'=>'Y'));
    	$this->db->order_by('ot_name', 'ASC');
    	$res = $this->db->get('umh_m9l2t0_our_team');
    	return $res;
    }

    function get_faq()
    {
    	$this->db->where(array('is_active'=>'Y'));
    	$this->db->order_by('faq_ask', 'ASC');
    	$res = $this->db->get('umh_m9l2t0_faq');
    	return $res;
    }

    function get_policy($id)
    {
        $this->db->where(array('policy_type'=>$id, 'is_active'=>'Y'));
        return $this->db->get('umh_m9l2t0_policy')->row();
    }

	function check_detail() {
		$username = $this->input->post('username');
      	$password = md5($this->input->post('passwd'));

      	$this->db->select("A.id_users, A.username, A.passwd, A.NIP, CONCAT(A.firstname, ' ', coalesce(A.lastname, '')) AS fullname, A.id_level, B.levelname, B.menu_access, A.image_profile");
		$this->db->from("tbl_users A");
		$this->db->join("tbl_level B", "A.id_level = B.id_level", "LEFT");
		$this->db->where("A.username", $username);
		$this->db->where("A.passwd", $password);
		$this->db->where("A.is_active", "Y");
		
		
		
		$result = $this->db->get();

      	if ($result->num_rows() > 0) {
			$ess_ = array('logged_in'=>TRUE);
			// Action insert success log
			$log_result = 'success login';
			$log_data = $this->uri->uri_string();
			//$this->lib_log->insert_log($log_result, $log_data);
			return  array_merge($ess_, $result->row_array());
      	} else {
			// Action insert failed login
			$log_result = 'failed login';
			$log_data = $this->uri->uri_string();
			//$this->lib_log->insert_log($log_result, $log_data);
			
      		return FALSE;
      	}
	}
	
	function update_online($id_users, $key){
		$this->db->set('is_login', $key);
		$this->db->where('id_users', $id_users);
		$this->db->update('tbl_users');
	}
	
	function cpassword() {
		$this->db->select('*');
		$this->db->from('tbl_users');
		$result = $this->db->get();
		return $result->row_array();

	}
	
	public function getPass($id){
		$this->db->where('id_users', $id);
		$q = $this->db->get('tbl_users');
		return $q->row();
    }
	
	function uPass($save){
		$this->db->where('id_users', $this->session->userdata('id_users'));
		$this->db->update("tbl_users",$save);
	}

    function save_subs($save) {
        $res = $this->db->insert('umh_m9l2t0_subscribe', $save);
        return $res;
    }

    function check_emailsubs($email) {
        $this->db->where(array('email' => $email, 'is_active' => 'Y'));
        $res = $this->db->get('umh_m9l2t0_subscribe');

        return $res;
    }
}

/* End of file home_m.php */
/* Location: ./application/models/home_m.php */