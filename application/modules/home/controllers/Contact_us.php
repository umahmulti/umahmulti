<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_us extends CI_Controller {

	public function __construct(){
		parent::__construct();
	    $this->load->model('contact_us_m');
	}

	public function index()
	{
		
		$data['title'] = "Kontak Kami";
		$data['js'] = $this->load->view('kontak_kami/_js', '', TRUE);
		$data['body'] = $this->load->view('kontak_kami/_index', '', TRUE);
		$data['meta_tag'] = array('meta_key'    => "percetakan terdekat,usaha percetakan,percetakan buku,spanduk percetakan,bisnis percetakan,tempat percetakan terdekat,percetakan foto terdekat,percetakan buku yasin terdekat,percetakan banner terdekat,percetakan stiker terdekat,percetakan undangan terdekat,toko percetakan terdekat,percetakan al quran",
			                        'meta_desc'     => 'Jangan ragu untuk menghubungi kami, mari kita cari solusi bersama untuk memecahkan masalah !',
			                        'meta_img'      => config_item('umah').'img/umahmulti-big-logo.png',
			                        'meta_img_alt'  => 'Logo Umah Multi',
						'meta_url'  	=> base_url()

			                        );

		$this->load->view('layout/index', $data);
	}

	function mail_contact()
	{
		$post = $this->input->post();
		$config = array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'mail.umahmulti.biz.id', 
		    'smtp_port' => 465,
		    'smtp_user' => 'info@umahmulti.biz.id',
		    'smtp_pass' => '@UmahInfo123',
		    'smtp_crypto' => 'ssl',
		    'mailtype' => 'html',
		    'smtp_timeout' => '4',
		    'charset' => 'iso-8859-1',
		    'wordwrap' => TRUE
		);
	    $this->load->library('email', $config);
	    $this->email->set_newline("\r\n");
		$from = $config['smtp_user'];
        $to = "umahmulti@gmail.com";
        $name = $post['namePerson'];
        $email = $post['emailAdd'];
        $telp = $post['noTelp'];
        $subject = $post['sbjctMsg'];
        $message = $post['msgContent'];
        $msg = "Dear Admin,";
        $msg .= "<br><br> Nama : $name";
        $msg .= "<br> Email : $email";
        $msg .= "<br> Telp : $telp";
        $msg .= "<br> Pesan : $message";
        $msg .= "<br><br> Terima Kasih.";

        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($msg);

        $json = array('status'=> 0, 'error' => 'Mohon Maaf permintaan Anda tidak bisa diproses, Mohon hubungi kami melalui Email atau Whatsapp');
        if($this->email->send()) {
        	$json = array('status'=> 1, 'success' => 'Terima Kasih ! Pertanyaan anda sudah kami terima, Kami segera menghubungi Anda melalui Email atau Whatsapp');
        }

        echo json_encode($json);
	}

}

/* End of file Contact_us.php */
/* Location: ./application/controllers/Contact_us.php */