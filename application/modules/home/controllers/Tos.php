<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tos extends CI_Controller {

	public function __construct(){
		parent::__construct();
	    $this->load->model('home_m');
	}

	public function index()
	{

		$content['data_tos'] = $this->home_m->get_policy('1');

		$data['title'] = "Syarat & Ketentuan";
		$data['body'] = $this->load->view('syarat_ketentuan/_index', $content, TRUE);
		$data['meta_tag'] = array('meta_key'    => "percetakan terdekat,usaha percetakan,percetakan buku,spanduk percetakan,bisnis percetakan,tempat percetakan terdekat,percetakan foto terdekat,percetakan buku yasin terdekat,percetakan banner terdekat,percetakan stiker terdekat,percetakan undangan terdekat,toko percetakan terdekat,percetakan al quran",
			                        'meta_desc'     => 'Umah Multi memiliki beberapa ketentuan yang dibuat untuk menjaga keamanan dan kenyamanan pelanggan dalam penggunaan website.',
			                        'meta_img'      => config_item('umah').'img/umahmulti-big-logo.png',
			                        'meta_img_alt'  => 'Logo Umah Multi',
						'meta_url'  	=> base_url()
			                        );

		$this->load->view('layout/index', $data);
	}


}

/* End of file Tos.php */
/* Location: ./application/controllers/Tos.php */