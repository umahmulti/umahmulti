<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About_us extends CI_Controller {

	public function __construct(){
		parent::__construct();
	    $this->load->model('about_us_m');
	}

	public function index()
	{
		
		$content['about'] = $this->about_us_m->get_about('1');
		$content['about_content'] = $this->about_us_m->get_about('2');
		$content['our_team'] = $this->about_us_m->get_our_team();

		// $data['css'] = $this->load->view('tentang_kami/_css', '', TRUE);
		// $data['js'] = $this->load->view('tentang_kami/_js', '', TRUE);
		$data['title'] = "Tentang Kami";
		$data['body'] = $this->load->view('tentang_kami/_index', $content, TRUE);
		$data['meta_tag'] = array('meta_key'    => "percetakan terdekat,usaha percetakan,percetakan buku,spanduk percetakan,bisnis percetakan,tempat percetakan terdekat,percetakan foto terdekat,percetakan buku yasin terdekat,percetakan banner terdekat,percetakan stiker terdekat,percetakan undangan terdekat,toko percetakan terdekat,percetakan al quran",
			                        'meta_desc'     => 'Umah Multi adalah UMKM jasa cetak digital dan offset, jasa desain grafis, dan jasa pembuatan website yang dibangun sekumpulan pemuda kreatif.',
			                        'meta_img'      => config_item('umah').'img/umahmulti-big-logo.png',
			                        'meta_img_alt'  => 'Logo Umah Multi',
						'meta_url'  	=> base_url()

			                        );

		$this->load->view('layout/index', $data);
	}


}

/* End of file About_us.php */
/* Location: ./application/controllers/About_us.php */