<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
	    // $this->load->helper('form');
	    // $this->load->library('form_validation');
	    $this->load->model('home_m');
	}

	public function index()
	{
		// if ($this->session->has_userdata('logged_in')) {
		// 	redirect('dashboard');
		// } else {
		// 	$this->load->view('_login');
		// }
		$content['image'] = $this->l_option->get_img('1');
		$content['category'] = $this->l_option->get_category();
		$content['service'] = $this->home_m->get_service();
		$content['cust_review'] = $this->home_m->get_cust_review();

		$data['css'] = $this->load->view('beranda/_css', '', TRUE);
		$data['js'] = $this->load->view('beranda/_js', '', TRUE);
		$data['body'] = $this->load->view('beranda/_index', $content, TRUE);
		$data['meta_tag'] = array('meta_key'    => "percetakan terdekat,usaha percetakan,percetakan buku,spanduk percetakan,bisnis percetakan,tempat percetakan terdekat,percetakan foto terdekat,percetakan buku yasin terdekat,percetakan banner terdekat,percetakan stiker terdekat,percetakan undangan terdekat,toko percetakan terdekat,percetakan al quran",
			                        'meta_desc'     => 'Jasa percetakan banner,spanduk,buku,undangan dan lainnya flat ongkir JaDeTaBek Rp 10.000*.Kami juga menyediakan Jasa Desain Grafis dan Website.',
			                        'meta_img'      => config_item('umah').'img/umahmulti-big-logo.png',
			                        'meta_img_alt'  => 'Logo Umah Multi',
						'meta_url'  	=> base_url()

			                        );

		$this->load->view('layout/index', $data);
	}

	function en()
	{
		$name = $this->uri->segment(3);
		$res = $this->l_option->encode($name);
		echo $res;
		exit;
	}

	function dec()
	{
		$name = $this->uri->segment(3);
		$res = $this->l_option->decode($name);
		echo $res;
		exit;
	}

	function un()
	{
		$res = $this->l_option->generate_unique_id(10);
		echo $res;
		exit;
	}

	function sign_in() {
		$this->form_validation->set_rules('username', 'NIP', 'required');
		$this->form_validation->set_rules('passwd', 'Password', 'required');
		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('warning', 'You Have To Fill Input Authentication');
			redirect('auth');
		} else {
			$user_log = $this->auth_m->check_detail();

			if(!$user_log) {
				$this->session->set_flashdata('error', 'Invalid Username/ Password or You dont have authorization. Please contact Administrator !');
				redirect('auth');
			} else {
				// remember me
                if($this->input->post("remember")!='') {
                  setcookie ("loginId", $user_log['username'], time()+ (365 * 24 * 60 * 60));  
                  // setcookie ("loginPass", $user_log['passwd'],  time()+ (365 * 24 * 60 * 60));
                } else {
                  setcookie ("loginId",""); 
                  // setcookie ("loginPass","");
                }                    

				$this->session->set_userdata($user_log);
				$this->update_online($this->session->userdata('id_users'), 'Y');
				$this->session->set_flashdata('success', 'Welcome back, '.$this->session->userdata('fullname').'!');
				redirect('dashboard');
			}
		}
	}

	function update_online($id_users, $key){
		$this->auth_m->update_online($id_users, $key);
	}
	
	function sign_out() {
		$this->update_online($this->session->userdata('id_users'), 'N');	//update ke ofline
	    $this->session->unset_userdata('logged_in');
	    $this->session->sess_destroy();
	    $this->session->set_flashdata('success', 'Thank You, You have been successfuly logged out!');
	    redirect('auth');
	}
	
	function cpass() {
		$data = $this->auth_m->cpassword();
		$this->load->view('auth/_form', $data);
    }
	
	public function checkPass($oldpass) {
		$id		= $this->session->userdata('id_users');
		$user	= $this->auth_m->get_user($id);
		
		if($user->passwd !== md5($oldpass)) {
			$this->form_validation->set_message('password_check', 'The {field} does not match');
            return false;
        }

        return true;
    }
	
	function save_password(){
		$id				= $this->session->userdata('id_users');
		$opass			= $this->input->post('oPass');
        $nPass			= $this->input->post('nPass');
        $passwd_desc	= $this->input->post('nPass');

        if ($opass == ''){
            $json = array('status'=> 0, 'error'=>'Password blank !');
            echo json_encode($json);
		}elseif ($nPass == ''){
            $json = array('status'=> 0, 'error'=>'Repeat Password blank !');
            echo json_encode($json);		
        }elseif ($passwd_desc == ''){
            $json = array('status'=> 0, 'error'=>'Repeat Password blank !');
            echo json_encode($json);		
        }else{
			$cPass		= $this->auth_m->getPass($id);
			if($cPass->passwd !== md5($opass)) {
				$json = array('status'=> 0, 'error'=>'Old Password not match !');
				echo json_encode($json);
			}else{
				$save = array(
					//'passwd'			=> $passwd,
					'passwd'			=> md5($nPass),
					'passwd_desc'		=> $passwd_desc,
					'last_modify_date'	=> date('Y-m-d H:i:s'),
					'last_modify_by'	=> $this->session->userdata('id_users')
				);
				$this->auth_m->uPass($save);
				$json = array('status'=> 1, 'success'=>'Success change password !');
				
				//$this->session->sess_destroy();
				echo json_encode($json);
			}
        }
    }

    function save_subs()
    {
    	$email = $this->input->post("emailSubs");
    	$subs_id = $this->l_option->generate_unique_id(10);

    	$check_emailsubs = $this->home_m->check_emailsubs($email);

    	$json = array('status'=> 2, 'warning' => 'Mohon Maaf data anda sudah teraftar di sistem kami !');
    	if($check_emailsubs->num_rows() <= 0) {
	    	$save = array(
	    				'id_subscribe'	=> $subs_id,
	    				'email'			=> $email,
		    			'created_date'	=> date('Y-m-d H:i:s'),
		    			'is_active'		=> 'Y'
	    			);

	    	$res = $this->home_m->save_subs($save);

	    	$json = array('status'=> 0, 'error' => 'Mohon Maaf data anda tidak tersimpan ke sistem kami, Mohon hubungi kami melalui Email atau Whatsapp');
	    	if($res) {
	    		$json = array('status'=> 1, 'success' => 'Terima Kasih! Telah berlangganan untuk mendapatkan promo terbaru dari kita!');
	    	}
	    }

    	echo json_encode($json);
    }

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */