<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends CI_Controller {

	public function __construct(){
		parent::__construct();
	    $this->load->model('faq_m');
	}

	public function index()
	{
		
		$content['data_faq'] = $this->faq_m->get_faq();

		$data['title'] = "Pertanyaan Umum";
		$data['body'] = $this->load->view('pertanyaan_umum/_index', $content, TRUE);
		$data['meta_tag'] = array('meta_key'    => "percetakan terdekat,usaha percetakan,percetakan buku,spanduk percetakan,bisnis percetakan,tempat percetakan terdekat,percetakan foto terdekat,percetakan buku yasin terdekat,percetakan banner terdekat,percetakan stiker terdekat,percetakan undangan terdekat,toko percetakan terdekat,percetakan al quran",
			                        'meta_desc'     => 'Berikut adalah beberapa pertanyaan yang sering diajukan oleh pelanggan mengenai jasa cetak digital, jasa desain grafis, dan jasa pembuatan website.',
			                        'meta_img'      => config_item('umah').'img/umahmulti-big-logo.png',
			                        'meta_img_alt'  => 'Logo Umah Multi',
						'meta_url'  	=> base_url()

			                        );

		$this->load->view('layout/index', $data);
	}


}

/* End of file Faq.php */
/* Location: ./application/controllers/Faq.php */