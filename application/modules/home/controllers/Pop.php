<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pop extends CI_Controller {

	public function __construct(){
		parent::__construct();
	    $this->load->model('home_m');
	}

	public function index()
	{
		
		$content['data_pop'] = $this->home_m->get_policy('2');
		
		$data['title'] = "Kebijakan Privasi";
		$data['body'] = $this->load->view('kebijakan_privasi/_index', $content, TRUE);
		$data['meta_tag'] = array('meta_key'    => "percetakan terdekat,usaha percetakan,percetakan buku,spanduk percetakan,bisnis percetakan,tempat percetakan terdekat,percetakan foto terdekat,percetakan buku yasin terdekat,percetakan banner terdekat,percetakan stiker terdekat,percetakan undangan terdekat,toko percetakan terdekat,percetakan al quran",
			                        'meta_desc'     => 'Umah Multi memiliki ketentuan kebijakan privasi untuk melindungi data-data pelanggannya, beberapa data dapat kami gunakan untuk pengembangan produk.',
			                        'meta_img'      => config_item('umah').'img/umahmulti-big-logo.png',
			                        'meta_img_alt'  => 'Logo Umah Multi',
						'meta_url'  	=> base_url()
			                        );

		$this->load->view('layout/index', $data);
	}


}

/* End of file Pop.php */
/* Location: ./application/controllers/Pop.php */