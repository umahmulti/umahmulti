<!-- Section About -->
<section id="about" class="about py-5">
  <!-- Desktop Display -->
  <div class="d-none d-md-block in-left">
    <div class="fold">          
      <img class="img-fold" src="<?php echo config_item('umah') ?>img/about-bg-fold.png" loading="lazy" alt="Tentang Umah Multi" title="Tentang Umah Multi">
      <div class="row py-5 px-5">
        <div class="col-md-6 text-center">
          <img class="img-about" src="<?php echo config_item('umah') ?>img/umahmulti-big-logo.png" loading="lazy" alt="Logo Umah Multi" title="Logo Umah Multi">
        </div>
        <div class="col-md-6">
          <h1 class="display-4">Tentang Kami</h1>
          <p class="text-justify float-right">
            <?php echo $about->content; ?>
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- End Desktop Display -->
  <!-- Mobile Display -->
  <div class="container d-block d-md-none">
    <div class="row py-3">
      <div class="col-md-6 text-center py-3">
        <img src="<?php echo config_item('umah') ?>img/umahmulti-big-logo.png" loading="lazy" alt="Logo Umah Multi" title="Logo Umah Multi">
      </div>
      <div class="col-md-4 py-3">
        <p class="text-justify">
          <?php echo $about->content; ?>
        </p>
      </div>
    </div>
  </div>
  <!-- End Mobile Display -->
</section>
<section id="AboutContent" class="AboutContent text-justify">
  <div class="container in-left">
    <?php echo $about_content->content; ?>
  </div>
</section>
<!-- End Section About -->
<!-- Section Tim -->
<section id="tim" class="tim py-5">
  <div class="container in-left">
    <h1 class="text-center pt-3">Tim Kami</h1>
    <div class="card-group py-3">
      <?php
        if($our_team->num_rows() > 0) {
          foreach($our_team->result() as $row) {
      ?>
          <div class="card">
            <img src="<?php echo config_item('umah').'/img/'.$row->ot_img; ?>" class="card-img-top" alt="<?php echo $row->ot_img_alt; ?>" title="<?php echo $row->ot_img_alt; ?>">
            <div class="card-body text-center">
              <h5 class="card-title"><?php echo $row->ot_name; ?></h5>
              <p class="card-text"><?php echo $row->ot_desc; ?></p>
            </div>
            <div class="card-footer">
              <small class="text-muted">Last updated <?php echo $this->l_option->time_elapsed_string($row->modified_date); ?></small>
            </div>
          </div>
      <?php
          }
        }
      ?>
    </div>
  </div>
</section>
<!-- End Section Tim -->