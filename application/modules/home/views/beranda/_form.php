	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form id="cPass">
				<div class="modal-body">	
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="text-info"> Old Password </label>
								<input type="password" class="form-control" name="oPass" placeholder="Old Password should not be empty !" required>
							</div>
						</div>
						<!--/span-->
						
						<div class="col-md-12">
							<div class="form-group">
								<label class="text-info"> New Password </label>
								<input type="password" class="form-control" name="nPass"  placeholder="Password should not be empty !">
							</div>
						</div>
						
						<div class="col-md-12">
							<div class="form-group">
								<label class="text-info"> Repeat New Password </label>
								<input type="password" class="form-control" name="rPass"  placeholder="Password should not be empty !" required >
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
				</div>
			
				<div class="modal-footer">
					<div class="box-footer p-0 text-right bg-light">
						<button type="submit" class="btn btn-success"> Submit </button>
						<button type="button" class="btn btn-danger" data-dismiss="modal"> Close </button>
					</div>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	
	<script>
		$('#cPass').submit(function() {
			$.ajax({
				type: 'POST',
				url: '<?php echo site_url('auth/save_password'); ?>',
				datatype:'json',
				data: $(this).serialize(),
				success: function(data) {
					jsonData = $.parseJSON(data);
					if(jsonData.status == 0) {
						$.toast({
							heading: 'Alert !',
							text: jsonData.error ,
							position: 'top-right',
							loaderBg: '#ff6849',
							icon: 'error',
							hideAfter: 6500,
							stack: 6
						});
						return false;
					} else {
						$.toast({
							heading: 'Success !',
							text: jsonData.success ,
							position: 'top-right',
							loaderBg: '#ff6849',
							icon: 'success',
							hideAfter: 6500,
							stack: 6
						});
						$('#centerModal').modal('hide');
					}
				}
			});
			return false;
		});
</script>