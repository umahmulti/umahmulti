<!-- Hero Carousel -->

<section id="hero" class="hero">

  <div id="carouselHero" class="carousel slide" data-ride="carousel">

    <ol class="carousel-indicators">

      <?php

        if($image->num_rows() > 0) {

          $no_img = 0;

          $class = '';

          foreach($image->result() as $row_img) {

            $class = ($no_img==0?'class="active"':'');

      ?>

        <li data-target="#carouselHero" data-slide-to="<?php echo $no_img; ?>" <?php echo $class; ?>></li>

      <?php

            $no_img++;

          }

        }

      ?>

    </ol>

    <div class="carousel-inner">

      <?php

        if($image->num_rows() > 0) {

          $act_intro = 'active';

          foreach($image->result() as $row_img) {

      ?>

        <div class="carousel-item <?php echo $act_intro; ?>">

          <img src="<?php echo base_url().'upload/img/slide/'.$row_img->img_name; ?>" loading="lazy" class="d-block w-100" alt="<?php echo $row_img->img_alt; ?>" title="<?php echo $row_img->img_alt; ?>">

          <div class="black-overlay"></div>

        </div>

      <?php

            $act_intro = '';

          }

        }

      ?>

    </div>

    <a class="carousel-control-prev" href="#carouselHero" role="button" data-slide="prev">

      <span class="carousel-control-prev-icon" aria-hidden="true"></span>

      <span class="sr-only">Previous</span>

    </a>

    <a class="carousel-control-next" href="#carouselHero" role="button" data-slide="next">

      <span class="carousel-control-next-icon" aria-hidden="true"></span>

      <span class="sr-only">Next</span>

    </a>

  </div>

</section>

<!-- End Hero Carousel -->

<!-- Section Pricing -->

<section id="pricing" class="pricing py-5 in-top">

  <div class="container text-center">

    <h1>Layanan Umah Multi</h1>

  </div>

  <div class="container py-5">

    <ul class="nav nav-tabs mb-3" id="priceTab" role="tablist">

      <?php

        if($category->num_rows() > 0) {

          $act_cat = 'active';

          foreach($category->result() as $row_navcat) {

            $control = $this->l_option->decode($row_navcat->id_category);

      ?>

        <li class="nav-item" role="presentation">

          <a class="nav-link <?php echo $act_cat; ?>" id="<?php echo $control; ?>-tab" data-toggle="tab" href="#<?php echo $control; ?>" role="tab" aria-controls="<?php echo $control; ?>" aria-selected="true"><h3><?php echo $row_navcat->cat_name; ?></h3></a>

        </li>

      <?php

            $act_cat = '';

          }

        }

      ?>

    </ul>

    <div class="tab-content" id="priceTabContent">

      <?php

        if($category->num_rows() > 0) {

          $show = 'show';

          $act_catprice = 'active';

          foreach($category->result() as $row_cat) {

            $control = $this->l_option->decode($row_cat->id_category);

            $total_product = $this->home_m->count_products($control);

      ?>

        <div class="tab-pane fade <?php echo $show.' '.$act_catprice; ?>" id="<?php echo $control; ?>" role="tabpanel" aria-labelledby="<?php echo $control; ?>-tab">

          <!-- Price Print Desktop -->

          <?php

            $product_mbl = $this->l_option->get_products('',$control);

            if($control != 'jasa-web') {

              $for_div = $total_product/3;

              $for_div = floor($for_div);

          ?>

          <div id="carouselPricing" class="carousel carousel-pricing slide d-none d-sm-block" data-ride="carousel">

            <div class="carousel-inner">

              <?php

                $act_prod = 'active';

                $limit = 1;

                for($i=0; $i < $for_div; $i++) {

                  $product = $this->l_option->get_products('',$control,$limit);

              ?>

              <div class="carousel-item <?php echo $act_prod; ?>">

                <div class="card-deck text-center in-down">

                  <?php

                    if($product->num_rows() > 0) {

                      foreach($product->result() as $row_prod) {

                        $parent = $this->home_m->get_parent($row_prod->parent);

                        $catmenu = $this->l_menu->get_menu('', $parent->menu_parent);

                        $prod_name = strtolower(str_replace(' ', '-', $row_prod->product_name));

                        $produrl = base_url().'produk/'.$this->l_option->decode($catmenu->id_menu).'/'.$prod_name;

                  ?>

                  <div class="card shadow-sm">

                    <div class="card-header">

                      <h2 class="my-0 font-weight-normal"><?php echo $row_prod->product_name; ?></h2>

                    </div>

                    <div class="card-body product">

                      <!--<h3 class="text-center">Mulai dari </h3>-->

                      <!--<h3 class="card-title pricing-card-title">Rp. <?php echo $row_prod->price; ?>* <small class="text-muted">/ <?php echo $row_prod->unit; ?></small></h3>-->

                        <img class="img-hover pb-3" src="<?php echo base_url().'upload/img/produk/'.$row_prod->product_img; ?>" loading="lazy" alt="<?php echo $row_prod->product_img_alt; ?>" title="<?php echo $row_prod->product_img_alt; ?>">

                      <a href="<?php echo $produrl; ?>"><button type="button" class="btn btn-lg btn-block btn-primary">Order Sekarang</button></a>

                    </div>

                  </div>

                  <?php

                      }

                    }

                  ?>

                </div>

              </div>

              <?php

                  $act_prod = '';

                  if($limit==1) {

                    $limit = $limit * 0;

                  }

                  $limit = $limit+3;

                }

              ?>

            </div>

            <a class="carousel-control-prev" href="#carouselPricing" role="button" data-slide="prev">

              <span class="carousel-control-prev-icon-price" aria-hidden="true"></span>

              <i class="fa fa-chevron-left" aria-hidden="true"></i>

              <span class="sr-only">Previous</span>

            </a>

            <a class="carousel-control-next" href="#carouselPricing" role="button" data-slide="next">

              <span class="carousel-control-next-icon-price" aria-hidden="true"></span>

              <i class="fa fa-chevron-right" aria-hidden="true"></i>

              <span class="sr-only">Next</span>

            </a>

          </div>

          <?php

            } else {

          ?>

          <div class="container d-none d-sm-block">

            <div class="row">

              <?php

                $prod_web = $this->l_option->get_products('', $control);

                if($prod_web->num_rows() > 0) {

                  foreach($prod_web->result() as $row_prod_web) {

                    $prod_web_name = strtolower(str_replace(' ', '-', $row_prod_web->product_name));

                    $produrl_web = base_url().'produk/jasa/'.$prod_web_name;

              ?>

                <div class="col-md-4 my-5">

                  <img src="<?php echo base_url().'upload/img/produk/'.$row_prod_web->product_img; ?>" loading="lazy" alt="<?php echo $row_prod_web->product_img_alt; ?>" title="<?php echo $row_prod_web->product_img_alt; ?>">

                  <div class="overlay">

                    <div class="text-jw">

                      <a href="<?php echo $produrl_web; ?>"><h5 class="text-center"><?php echo $row_prod_web->product_name; ?></h5></a>

                      <ul class="list-unstyled">

                        <?php

                          if($row_prod_web->product_desc!=null || $row_prod_web->product_desc!='') {

                            $arr_desc = explode(',', $row_prod_web->product_desc);

                            foreach($arr_desc as $rowdesc) {

                        ?>

                            <li><i class="fa fa-check" aria-hidden="true"><?php echo $rowdesc; ?></i></li>

                        <?php

                            }

                          }

                        ?>

                      </ul>

                      <?php if($row_prod_web->m9l2t0_product_id == '26') { ?>

                        <p class="text-center" ><b>Mulai dari Rp. <?php echo $row_prod_web->price; ?>/ <?php echo $row_prod_web->unit; ?></b></p>

                      <?php } else { ?>

                        <p><b>Rp. <?php echo $row_prod_web->price; ?>/ <?php echo $row_prod_web->unit; ?></b></p>

                      <?php } ?>

                    </div>

                  </div>

                </div>

              <?php

                  }

                }

              ?>

            </div>

          </div>

          <?php } ?>

          <!-- End Price Print Desktop -->

          <!-- Price Print Mobile -->

          <div id="<?php echo $control; ?>Mbl" class="carousel carousel-pricing slide d-block d-md-none" data-ride="carousel">

            <div class="carousel-inner">

              <?php

                if($product_mbl->num_rows() > 0) {

                  $product_m = $this->l_option->get_products('',$control);

                  $act_prod_m = 'active';

                  foreach($product_m->result() as $row_prod_m) {

                    if($control != 'jasa-web') {

                      $parent_m = $this->home_m->get_parent($row_prod_m->parent);

                      $catmenu_m = $this->l_menu->get_menu('', $parent_m->menu_parent);

                    }

                    $prod_name_m = strtolower(str_replace(' ', '-', $row_prod_m->product_name));

                    if($control != 'jasa-web') {

                      $produrl_m = base_url().'produk/'.$this->l_option->decode($catmenu_m->id_menu).'/'.$prod_name_m;

                    } else {

                      $produrl_m = base_url().'produk/jasa/'.$prod_name_m;

                    }

              ?>

                <div class="carousel-item text-center <?php echo $act_prod_m; ?>">

                  <div class="card shadow-sm">

                    <div class="card-header">

                      <h2 class="my-0 font-weight-normal"><?php echo $row_prod_m->product_name; ?></h2>

                    </div>

                    <div class="card-body product">

                        <!--<h3>Mulai dari </h3>-->

                        <!--<h3 class="card-title pricing-card-title">Rp. <?php echo $row_prod_m->price; ?>* <small class="text-muted">/ <?php echo $row_prod_m->unit; ?></small></h3>-->

                        <img class="img-hover pb-3" src="<?php echo base_url().'upload/img/produk/'.$row_prod_m->product_img; ?>" loading="lazy" alt="<?php echo $row_prod_m->product_img_alt; ?>" title="<?php echo $row_prod_m->product_img_alt; ?>">

                      <a href="<?php echo $produrl_m; ?>"><button type="button" class="btn btn-lg btn-block btn-primary">Order Sekarang</button></a>

                    </div>

                  </div>

                </div>

              <?php

                    $act_prod_m = '';

                  }

                }

              ?>

            </div>

            <a class="carousel-control-prev" href="#<?php echo $control; ?>Mbl" role="button" data-slide="prev">

              <span class="carousel-control-prev-icon-price" aria-hidden="true"></span>

              <i class="fa fa-chevron-left" aria-hidden="true"></i>

              <span class="sr-only">Previous</span>

            </a>

            <a class="carousel-control-next" href="#<?php echo $control; ?>Mbl" role="button" data-slide="next">

              <span class="carousel-control-next-icon-price" aria-hidden="true"></span>

              <i class="fa fa-chevron-right" aria-hidden="true"></i>

              <span class="sr-only">Next</span>

            </a>

          </div>

          <!-- End Price Print Mobile -->

        </div>

      <?php

            $act_catprice = '';

            $show = '';

          }

        }

      ?>

    </div>

  </div>

</section>

<!-- End Section Pricing -->

<!-- Section Subscribe -->

<section id="subscribe" class="subscribe">

  <div class="container container-swing d-none d-md-block">

    <img src="<?php echo config_item('umah'); ?>img/subs-img.png" loading="lazy" alt="Subscribe Umah Multi" title="Subscribe Umah Multi">

    <div class="centered">

      <h1>Yuk Ikuti Kami !</h1>

      <p>Untuk mendapatkan info menarik.</p>

      <form id="subsForm" class="form-element" method="POST">

        <div class="row py-4">

          <div class="col-md-8">

            <div class="form-group">

              <input type="email" id="emailSubs" name="emailSubs" class="form-control form-control-lg" placeholder="name@example.com" required>

            </div>

          </div>

          <div class="col-md-4">

            <button type="submit" class="btn btn-primary btn-lg float-left">Kirim <i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>

          </div>

        </div>

      </form>

    </div>

  </div>

  <div class="container d-block d-md-none pt-5">

    <div class="card">

      <h5 class="card-header">Ikuti Kami ! Untuk mendapatkan info menarik.</h5>

      <div class="card-body">

        <form id="subsForm" class="form-element" method="POST">

          <div class="row py-4">

            <div class="col-md-8">

              <div class="form-group">

                <input type="email" id="emailSubs" name="emailSubs" class="form-control form-control-lg" placeholder="name@example.com">

              </div>

            </div>

            <div class="col-md-4">

              <button type="submit" class="btn btn-primary btn-lg float-right">Kirim <i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>

            </div>

          </div>

        </form>

      </div>

    </div>

  </div>

</section>

<!-- End SectionSubscribe -->

<!-- Section Featured -->

<section id="featured" class="featured py-5">

  <div class="container text-center">

    <h1>Coba Penawaran Kami !</h1>

  </div>

  <div class="container text-center">

    <div class="row">

      <?php

        if($service->num_rows() > 0) {

          foreach($service->result() as $row_serv) {

      ?>

        <div class="col my-3">

          <img src="<?php echo config_item('umah').'img/'.$row_serv->serv_img; ?>" alt="<?php echo $row_serv->serv_img_alt; ?>" title="<?php echo $row_serv->serv_img_alt; ?>" class="shadow-lg">

        </div>

      <?php

          }

        }

      ?>

    </div>

  </div>

</section>

<!-- End Section Featured -->

<!-- Section Porfolio -->

<section id="portfolio" class="portfolio py-5 in-top">

  <div class="container text-center py-5">

    <h1>Apa Kata Pelanggan Kami ?</h1>

  </div>

  <div class="container">

    <div class="card-columns">

      <?php

        if($cust_review->num_rows() > 0) {

          foreach($cust_review->result() as $row_cus_rev) {

      ?>

        <div class="card text-center">

          <img class="card-img-top" src="<?php echo config_item('umah').'img/'.$row_cus_rev->cr_img; ?>" loading="lazy" alt="<?php echo $row_cus_rev->cr_img_alt; ?>" title="<?php echo $row_cus_rev->cr_img_alt; ?>">

          <div class="card-body">

            <h5 class="card-title"><?php echo $row_cus_rev->cr_name; ?></h5>

            <p class="card-text"><?php echo $row_cus_rev->cr_desc; ?></p>

            <p class="card-text"><small class="text-muted">Last updated <?php echo $this->l_option->time_elapsed_string($row_cus_rev->created_date); ?></small></p>

          </div>

        </div>

      <?php

          }

        }

      ?>

    </div>

  </div>

</section>

<!-- End Section Portfolio -->