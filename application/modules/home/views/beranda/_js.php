<script>
	$('#subsForm').on('submit', function(e) {
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: '<?php echo site_url('home/save_subs'); ?>',
			datatype: 'json',
			data: $(this).serialize(),
			success: function(data) {
				jsonData = $.parseJSON(data);
				if(jsonData.status == 0) {
					$.toast({
						heading: 'Alert !',
						text: jsonData.error ,
						position: 'top-right',
						loaderBg: '#ff6849',
						icon: 'error',
						hideAfter: 6500,
						stack: 6
					});
				} else if(jsonData.status == 2) {
					$.toast({
						heading: 'Warning !',
						text: jsonData.warning ,
						position: 'top-right',
						loaderBg: '#ff6849',
						icon: 'warning',
						hideAfter: 6500,
						stack: 6
					});
				} else {
					$.toast({
						heading: 'Success !',
						text: jsonData.success ,
						position: 'top-right',
						loaderBg: '#ff6849',
						icon: 'success',
						hideAfter: 6500,
						stack: 6
					});

					$('#emailSubs').val('');
				}
			}
		});
	});
</script>