<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="<?php echo config_item('CCS'); ?>images/favicon.ico">
		
		<title>Call Center Solutions - Log in </title>
		
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="<?php echo config_item('CCS'); ?>vendor_components/bootstrap/dist/css/bootstrap.min.css">
		
		<!-- Bootstrap extend-->
		<link rel="stylesheet" href="<?php echo config_item('CCS'); ?>css/bootstrap-extend.css">
		
		<!-- toast CSS -->
		<link href="<?php echo config_item('CCS'); ?>vendor_components/jquery-toast-plugin-master/src/jquery.toast.css" rel="stylesheet">
		
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo config_item('CCS'); ?>css/master_style.css">
		
		<!-- Superieur Admin skins -->
		<link rel="stylesheet" href="<?php echo config_item('CCS'); ?>css/skins/_all-skins.css">
	</head>
	
	
	<body class="hold-transition bg-img" style="background-image: url(<?php echo config_item('CCS'); ?>images/gallery/full/login-bg.jpg)" data-overlay="4">
		<div class="auth-2-outer row align-items-center h-p100 m-0">
			<div class="auth-2">
				<div class="auth-logo font-size-40">
					<a href="#" class="text-white"><b> Call Center </b> Solutions </a>
				</div>
				
				<!-- /.login-logo -->
				
				<div class="auth-body">
					<p class="auth-msg">Sign in to start your session</p>
					
					<?php echo form_open('auth/sign_in', array('class' => 'form-element', 'id' => 'login_form')) ?>
						<div class="form-group has-feedback">
							<input type="text" class="form-control" name="username" placeholder="Username !" value="<?php echo (isset($_COOKIE['loginId'])?$_COOKIE['loginId']:''); ?>" autofocus>
							<span class="ion ion-email form-control-feedback"></span>
						</div>
						
						<div class="form-group has-feedback">
							<input type="password" class="form-control" name="passwd" placeholder="Password !">
							<span class="ion ion-locked form-control-feedback"></span>
						</div>
						
						<div class="row">
							<div class="col-12 text-center">
								<button type="submit" class="btn btn-block mt-10 btn-success">SIGN IN</button>
							</div>
							<!-- /.col -->
						</div>
					</form>
					
					<!-- <div class="margin-top-30 text-center">
						<p>Don't have an account? <a href="#" class="text-info m-l-5">Sign Up</a></p>
					</div> -->
					
					<div class="text-center text-white">
						<p class="mt-50">- Watch us on  -</p>
						<p class="gap-items-2 mb-20">
							<a class="btn btn-social-icon btn-outline btn-white" href="http://www.trans-cosmos.co.id/"><i class="fa fa-internet-explorer "></i></a>
							<a class="btn btn-social-icon btn-outline btn-white" href="https://www.facebook.com/TranscosmosIndonesia/"><i class="fa fa-facebook"></i></a>
							<a class="btn btn-social-icon btn-outline btn-white" href="https://twitter.com/transcosmosid"><i class="fa fa-twitter"></i></a>
							<a class="btn btn-social-icon btn-outline btn-white" href="https://id.linkedin.com/company/transcosmos-indonesia"><i class="fa fa-linkedin-square"></i></a>
							<a class="btn btn-social-icon btn-outline btn-white" href="https://www.instagram.com/transcosmos.id/?hl=id"><i class="fa fa-instagram"></i></a>
						</p>
					</div>
					<!-- /.social-auth-links -->					
				</div>
			</div>
		</div>
	
		<!-- jQuery 3 -->
		<script src="<?php echo config_item('CCS'); ?>vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
		
		<!-- popper -->
		<script src="<?php echo config_item('CCS'); ?>vendor_components/popper/dist/popper.min.js"></script>
		
		<!-- Bootstrap 4.0-->
		<script src="<?php echo config_item('CCS'); ?>vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

		<!-- toast -->
		<script src="<?php echo config_item('CCS'); ?>vendor_components/jquery-toast-plugin-master/src/jquery.toast.js"></script>
		<script src="<?php echo config_item('CCS'); ?>js/pages/notification.js"></script>
	
	
		<!-- Setting toast as default notify -->
		
		<script>
			<?php if($this->session->flashdata('info')) : ?>
				$.toast({
					heading: 'Information !',
					text: '<?php echo $this->session->flashdata('info'); ?>' ,
					position: 'top-right',
					loaderBg: '#ff6849',
					icon: 'info',
					hideAfter: 3000,
					stack: 6
				});
			<?php endif; ?>
			
			<?php if($this->session->flashdata('warning')) : ?>
				$.toast({
					heading: 'Warning !',
					text: '<?php echo $this->session->flashdata('warning'); ?>',
					position: 'top-right',
					loaderBg: '#c29648',
					icon: 'warning',
					hideAfter: 3500,
					stack: 6
				});
			<?php endif; ?>
			
			<?php if($this->session->flashdata('success')) : ?>
				$.toast({
					heading: 'Good !',
					text: '<?php echo $this->session->flashdata('success'); ?>',
					position: 'top-right',
					loaderBg: '#ff6849',
					icon: 'success',
					hideAfter: 3500,
					stack: 6
				});
			<?php endif; ?>
			
			<?php if($this->session->flashdata('error')) : ?>
				$.toast({
					heading: 'Error !',
					text: '<?php echo $this->session->flashdata('error'); ?>',
					position: 'top-right',
					loaderBg: '#ff6849',
					icon: 'error',
					hideAfter: 3500,
					stack: 6
				});
			<?php endif; ?>
		</script>

		
	</body>
</html>
