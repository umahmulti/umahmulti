<!-- Section TOS -->
<section id="tos-page" class="tos-page py-5">
  <div class="container">
    <div class="row in-left">
      <div class="col-lg-8 col-sm-12">
        <h1 class="display-4">Syarat dan Ketentuan Berlaku</h1>
        <p class="lead">Mohon baca dengan seksama, karena ini dapat membantu anda!</p>
      </div>
    </div>
  </div>
  <div class="container in-left border-top py-5">
    <?php echo $data_tos->content; ?>
  </div>
</section>
<!-- End Section TOS -->