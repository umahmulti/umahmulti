<!-- Section FAQ -->
<section id="faq-page" class="faq-page py-5">
  <div class="container">
    <div class="row in-left">
      <div class="col-lg-8 col-sm-12">
        <h1 class="display-4">Pertanyaan Umum</h1>
        <p class="lead">Ini adalah pertanyaan yang sering di ajukan oleh pelanggan, semoga membantu.</p>
      </div>
    </div>
  </div>
  <div class="container in-down border-top py-5">
    <!-- Accordion FAQs -->
    <div class="accordion mb-3" id="accordionFaqs">
      <?php
        if($data_faq->num_rows() > 0) {
          $no = 1;
          $show = "show";
          foreach($data_faq->result() as $row) {
      ?>
        <div class="card">
          <div class="card-header" id="heading<?php echo $no; ?>">
            <h2 class="mb-0">
              <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $no; ?>" aria-expanded="true" aria-controls="collapse<?php echo $no; ?>">
                <?php echo $row->faq_ask; ?>
              </button>
            </h2>
          </div>
          <div id="collapse<?php echo $no; ?>" class="collapse <?php echo $show; ?>" aria-labelledby="heading<?php echo $no; ?>" data-parent="#accordionFaqs">
            <div class="card-body">
              <?php echo $row->faq_desc; ?>
            </div>
          </div>
        </div>
      <?php
            $no++;
            $show = "";
          }
        }
      ?>
    </div>
    <!-- End Accordion FAQs -->
    <div class="alert alert-info text-center" role="alert">
      Jika ada pertanyaan lainnya silahkan <a href="<?php echo base_url('kontak-kami'); ?>" class="alert-link">Hubungi Kami!</a> untuk info lebih lanjut.
    </div>
  </div>
</section>
<!-- End Section FAQ -->