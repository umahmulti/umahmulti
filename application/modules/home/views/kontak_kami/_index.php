<!-- Section Contact -->
<section id="contact-page" class="contact-page py-5">
  <div class="container">
    <div class="row in-left">
      <div class="col-lg-8 col-sm-12">
        <h1 class="display-4">Hubungi Kami</h1>
        <p class="lead">Jangan ragu menghubungi kami, mari kita cari solusi bersama !</p>
      </div>
    </div>
  </div>
  <div class="container in-left border-top py-5">
    <div class="card">
      <div class="card-body">
        <form id="contactForm" class="form-element" method="POST">
          <div class="form-row pb-3">
            <div class="col">
              <input type="text" class="form-control" name="namePerson" placeholder="Nama">
            </div>
          </div>
          <div class="form-row py-3">
            <div class="col">
              <input type="email" class="form-control" name="emailAdd" placeholder="name@example.com">
            </div>
            <div class="col">
              <input type="number" class="form-control" name="noTelp" placeholder="No. Telp">
            </div>
          </div>
          <div class="form-row py-3">
            <div class="col">
              <input type="text" class="form-control" name="sbjctMsg" placeholder="Subjek Pesan">
            </div>
          </div>
          <div class="form-group">
            <textarea class="form-control" name="msgContent" rows="3" placeholder="Isi Pesan Anda"></textarea>
          </div>
          <button type="submit" class="btn btn-primary btn-lg float-right">Submit</button>
        </form>
      </div>
    </div>
  </div>
</section>
<!-- End Section Contact -->