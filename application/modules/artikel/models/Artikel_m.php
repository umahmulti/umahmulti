<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Artikel_m extends CI_Model {

	function get_all_artikel($tag=null)
	{
		if($tag!=null) {
			$this->db->like('art_meta_keyword', $tag);
		}
		$this->db->where(array('is_active'=>'Y'));
		$this->db->order_by('created_date', 'DESC');
		$this->db->limit(10);
		$res = $this->db->get('umh_m9l2t0_art');
		return $res;
	}

	function get_artikel($id)
	{
		$this->db->where(array('id_art'=>$id, 'is_active'=>'Y'));
		$res = $this->db->get('umh_m9l2t0_art');
		return $res->row();
	}

	function get_title_art($id)
	{
		$this->db->where(array('id_art'=>$id, 'is_active'=>'Y'));
		$res = $this->db->get('umh_m9l2t0_art');
		return $res->row()->art_title;
	}

	function get_title()
	{
		$this->db->where('is_active', 'Y');
		$res = $this->db->get('umh_m9l2t0_art');
		return $res;
	}
}

/* End of file artikel_m.php */
/* Location: ./application/models/artikel_m.php */