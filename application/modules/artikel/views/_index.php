<!-- Section Content -->
<section id="blog" class="blog py-5">
  <div class="container">
    <?php
      if(isset($data_tag)) {
    ?>
      <h1 class="display-4 in-left">#<?php echo $data_tag; ?></h1>
      <p class="lead in-left">Berikut adalah Artikel yang terkait :</p>
    <?php } else { ?>
      <h1 class="display-4 in-left">Artikel</h1>
      <p class="lead in-left">Semoga Artikel Yang Kami Buat Dapat Membantu Anda.</p>
    <?php } ?>
  </div>
  <div class="container border-top py-5">
    <div class="row">
      <?php
        if($data_artikel->num_rows() > 0) {
          foreach($data_artikel->result() as $row) {
            $title = strtolower(str_replace(' ', '-', $row->art_title));
            $url = base_url().'artikel/'.$title;
            $date_sitemap = ($row->modified_date!=''?$row->modified_date:$row->created_date);
            $this->l_option->ins_url($url, 0.8, $date_sitemap);
      ?>
        <div class="col-lg-6 my-2">
          <a href="<?php echo $url; ?>">
            <div class="card h-100 mb-3">
              <div class="row no-gutters">
                <div class="col-md-4">
                  <img src="<?php echo base_url().'upload/img/artikel/'.$row->art_meta_img; ?>" class="card-img" alt="<?php echo $row->art_meta_img_alt; ?>" title="<?php echo $row->art_meta_img_alt; ?>">
                </div>
                <div class="col-md-8">
                  <div class="card-body">
                    <h5 class="card-title"><?php echo $row->art_title; ?></h5>
                    <p class="card-text text-justify"><?php echo $row->art_meta_desc; ?></p>
                    <p class="card-text"><small class="text-muted">Last updated <?php echo $this->l_option->time_elapsed_string($row->modified_date); ?></small></p>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      <?php
          }
        }
      ?>
    </div>
  </div>
</section>
<!-- End Section Content -->