<section id="artPage" class="artPage py-5">
  <div class="container">
    <h1 class="display-4 in-left"><?php echo $detail_art->art_title; ?></h1>
    <p class="lead in-left">Semoga Artikel Yang Kami Buat Dapat Membantu Anda.</p>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-12 border-top border-right py-3 text-justify">
        <img src="<?php echo base_url().'upload/img/artikel/'.$detail_art->art_img; ?>" alt="<?php echo $detail_art->art_img_alt; ?>" title="<?php echo $detail_art->art_img_alt; ?>">
        <p class="py-3"><?php echo $detail_art->art_content; ?></p>
      </div>
      <div class="col-lg-4 col-md-12 border-top py-3">
        <div class="recUpdPost">
          <h4 class="pb-3">Artikel Terbaru</h4>
          <ul class="list-group list-group-flush">
            <?php
              if($data_artikel->num_rows() > 0) {
                foreach($data_artikel->result() as $row) {
                  if($row->id_art != $detail_art->id_art) {
                    $title = strtolower(str_replace(' ', '-', $row->art_title));
            ?>
              <li class="list-group-item"><a href="<?php echo base_url().'artikel/'.$title; ?>"><img src="<?php echo base_url().'upload/img/artikel/'.$row->art_meta_img; ?>" class="img-fluid img-thumbnail mr-3"><?php echo $row->art_title; ?></li></a>
            <?php
                  }
                }
              }
            ?>
          </ul>
        </div>
        <div class="tagsPost border-top py-3">
          <h4 class="py-3">Tag Terkait</h4>
            <?php
              $tags = explode(',', $detail_art->art_meta_keyword);
              foreach($tags as $row_tags) {
                $tag = strtolower(str_replace(' ', '-', $row_tags));
            ?>
              <a href="<?php echo base_url(); ?>artikel/tag/<?php echo $tag; ?>"><button type="button" class="btn btn-outline-dark"><?php echo $row_tags; ?></button></a>
            <?php } ?>
        </div>
        <div class="postFeature border-top py-3">
          <h4 class="py-3">Produk Terkait</h4>
          <div id="carouselProductArt" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <?php
                $produk = $this->l_option->get_products($detail_art->parent);
                if($produk->num_rows() > 0 ){
                  $active = 'active';
                  foreach($produk->result() as $row_prod) {
                    $sub_cat = $this->l_menu->get_menu('', $row_prod->parent);
                    $cat = $this->l_menu->get_menu('', $sub_cat->menu_parent);
                    $catname = strtolower(str_replace(' ', '-', $cat->menu_name));
                    $productname = strtolower(str_replace(' ', '-', $row_prod->product_name));

              ?>
                  <div class="carousel-item <?php echo $active; ?>">
                    <a href="<?php echo base_url(); ?>produk/<?php echo $catname; ?>/<?php echo $productname; ?>">
                      <img src="<?php echo base_url()."upload/img/produk/".$row_prod->product_img; ?>" class="img-thumbnail" alt="">
                      <div class="carousel-caption d-none d-md-block">
                        <h5><?php echo $row_prod->product_name; ?></h5>
                        <!--<p><?php echo $row_prod->product_desc_meta; ?></p>-->
                      </div>
                    </a>
                  </div>
              <?php
                    $active = '';
                  }
                }
              ?>
            </div>
            <a class="carousel-control-prev" href="#carouselProductArt" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon-price" aria-hidden="true"></span>
              <i class="fa fa-chevron-left" aria-hidden="true"></i>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselProductArt" role="button" data-slide="next">
              <span class="carousel-control-next-icon-price" aria-hidden="true"></span>
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>