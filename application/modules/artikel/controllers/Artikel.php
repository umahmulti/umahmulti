<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Artikel extends CI_Controller {

	public function __construct(){
		parent::__construct();
	    $this->load->model('artikel_m');
	}

	public function index($id = null)
	{
		$content['data_artikel'] = $this->artikel_m->get_all_artikel();

		if($id==null) {
			$data['title'] = "Artikel";
			$data['body'] = $this->load->view('_index', $content, TRUE);
			$data['meta_tag'] = array('meta_key'    => $this->implode_keyword(),
			                        'meta_desc'     => 'Artikel Umah Multi berisikan berbagai informasi mengenai jasa percetakan digital dan offset, jasa desain grafis dan jasa pembuatan website.',
			                        'meta_img'      => config_item('umah').'img/umahmulti-big-logo.png',
			                        'meta_img_alt'  => 'Logo Umah Multi',
			                        'meta_url'	=> base_url().'artikel'
			                        );
		} else {
			$idArt = 'art-'.$id;
			$idArt = $this->l_option->encode($idArt);
			$content['detail_art'] = $this->artikel_m->get_artikel($idArt);
			$art_title = $this->artikel_m->get_title_art($idArt);
			$title = strtolower(str_replace(' ', '-', $content['detail_art']->art_title));
            		$url = base_url().'artikel/'.$title;

			$data['title'] = $art_title;
			$data['body'] = $this->load->view('blog/_index', $content, TRUE);
			$data['meta_tag'] = array('meta_key'    => $content['detail_art']->art_meta_keyword,
			                        'meta_desc'     => $content['detail_art']->art_meta_desc,
			                        'meta_img'      => base_url().'upload/img/artikel/'.$content['detail_art']->art_meta_img,
			                        'meta_img_alt'  => $content['detail_art']->art_meta_img_alt,
			                        'meta_url'	=> $url
			                        );
		}

		$this->load->view('layout/index', $data);
	}

	function tag($id_tag = null)
	{
		$id_tag = str_replace('-', ' ', $id_tag);
		$content['data_artikel'] = $this->artikel_m->get_all_artikel($id_tag);
		$content['data_tag'] = $id_tag;

		$data['title'] = "Artikel - #".$id_tag;
		$data['body'] = $this->load->view('_index', $content, TRUE);

		$this->load->view('layout/index', $data);
	}

	function implode_keyword()
	{
		$res = $this->artikel_m->get_title();
		$art = array();
		if($res->num_rows() > 0) {
			foreach($res->result() as $row) {
				$art[] = $row->art_title;
			}
		}

		$art = implode(',',$art);

		return $art;
	}


}

/* End of file Artikel.php */
/* Location: ./application/controllers/Artikel.php */