/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 10.3.16-MariaDB : Database - db_umah
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`umah_multi_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `umah_multi_db`;

/*Table structure for table `umh_m9l2t0_about` */

DROP TABLE IF EXISTS `umh_m9l2t0_about`;

CREATE TABLE `umh_m9l2t0_about` (
  `id_about` varchar(10) NOT NULL,
  `content` text DEFAULT NULL,
  `loc` int(2) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_about`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_about` */

insert  into `umh_m9l2t0_about`(`id_about`,`content`,`loc`,`modified_by`,`modified_date`,`modified_ip`,`is_active`) values 
('G2BqM85Eun','Umah Multi adalah Usaha Mikro Kecil Menengah (UMKM) yang dibangun dari sekumpulan pemuda kreatif memiliki tujuan membangun dan memajukan perekonomian masyarakat. Umah Multi menyediakan berbagai macam jasa yang dibutuhkan untuk kebutuhan organisasi, kebutuhan pemasaran dan kebutuhan pribadi.',1,NULL,NULL,NULL,'Y'),
('unAJFlE1pU','<h3 class=\"py-3\">Berikut beberapa Jasa yang kami sediakan :</h3><h4>Jasa percetakan Digital dan Offset</h4><ul><li>Kebutuhan Organisasi</li><p>Dalam berorganisasi pastinya membutuhkan berbagai macam pendukung yang dapat dijadikan kelengkapan peralatan anggota hingga aksesoris yang dapat memperindah ruangan, Umah Multi menyediakan jasa cetak untuk kebutuhan organisasi anda antara lain :</p><p>Cetak kartu nama, cetak kartu RFID, cetak kalender dan lain sebagainya.</p><li>Kebutuhan Pemasaran</li><p>Dalam dunia pemasaran, seorang pengusaha pasti ingin produknya dapat dikenal oleh pelanggannya. Dan pastinya ia memerlukan media yang dapat mengantarkan produknya hingga ke pelanggan. Misal media cetak yang masih dibutuhkan hingga sekarang untuk pemasaran. Umah Multi menyediakan jasa cetak untuk kebutuhan pemasaran anda, antara lain:</p><p>Cetak banner, cetak majalah, cetak spanduk, cetak brosur dan lain sebagainya.</p><li>Kebutuhan Pribadi</li><p>Dalam kehidupan sehari-hari terkadang kita memerlukan sesuatu untuk memberikan informasi secara pribadi melalui media cetak, misalnya seperti undangan kepada kerabat dan lainnya. Umah Multi menyediakan jasa cetak untuk kebutuhan pribadi, antara lain :</p><p>Cetak kartu undangan pernikahan, cetak kartu undangan khitanan, dll.</p></ul><h4>Jasa Desain Grafis</h4><p>Desain grafis sangatlah dibutuhkan ketika ingin merepresentasikan sebuah objek ke dalam bentuk visual yang dimaksudkan untuk proses komunikasi didalamnya.Umah Multi yang dibangun oleh berbagai pemuda kreatif mampu memenuhi kebutuhan anda untuk memberikan desain grafis yang memiliki makna untuk memberikan kesan kepada pembacanya.</p><h4>Jasa Pembuatan Website (Web Company Profile, UMKM, Portfolio, dsb.)</h4><p>Website merupakan kebutuhan penting di zaman seperti ini, karena berbagai informasi yang ada disebarkan dengan cepat pada dunia internet. Website lah salah satunya yang dapat dijadikan media penyebaran informasi didalamnya. Bahkan website mampu dijadikan tempat komunikasi sosial bahkan perniagaan didalamnya.</p><p>Kami juga memiliki sistem pengantaran barang dari tim kami sendiri dan Cakupan wilayah pelayanan kami adalah JaDeTaBek.<b> Tenang saja ! biaya pengirimannya terjangkau, yakni flat Rp 5000 untuk wilayah Jakarta dan 10.000 untuk DeTaBek*.</b></p>',2,NULL,NULL,NULL,'Y');

/*Table structure for table `umh_m9l2t0_art` */

DROP TABLE IF EXISTS `umh_m9l2t0_art`;

CREATE TABLE `umh_m9l2t0_art` (
  `m9l2t0_article_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_art` varchar(200) NOT NULL,
  `art_title` varchar(100) DEFAULT NULL,
  `art_content` text DEFAULT NULL,
  `art_meta_desc` varchar(150) DEFAULT NULL,
  `art_meta_keyword` varchar(100) DEFAULT NULL,
  `art_img` varchar(100) DEFAULT NULL,
  `art_img_alt` varchar(50) DEFAULT NULL,
  `art_meta_img` varchar(100) DEFAULT NULL,
  `art_meta_img_alt` varchar(50) DEFAULT NULL,
  `parent` varchar(200) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_art`),
  KEY `m9l2t0_article_id` (`m9l2t0_article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_art` */

insert  into `umh_m9l2t0_art`(`m9l2t0_article_id`,`id_art`,`art_title`,`art_content`,`art_meta_desc`,`art_meta_keyword`,`art_img`,`art_img_alt`,`art_meta_img`,`art_meta_img_alt`,`parent`,`created_by`,`created_date`,`created_ip`,`modified_by`,`modified_date`,`modified_ip`,`is_active`) values 
(2,'cnpCMkxzb1V1SmZjMjRxWVV3L2xsQT09','Kartu RFID','<div class=\"text-justify\">\r\n	<h2>Pengertian, Fungsi, Jenis Material dan Harga Cetak Kartu RFID</h2>\r\n	<p>\r\n		Apakah anda memahami, apa itu Kartu RFID ? Apakah anda pernah melihat dan menggunakannya ? mungkin saja anda menggunakan Kartu RFID namun tidak menyangka kartu yang anda gunakan adalah Kartu RFID. Dalam kehidupan sehari-hari misalnya, seperti kartu e-money, flazz dan sebagainya.\r\n	</p>\r\n	<h2>Pengertian Kartu RFID ?</h2>\r\n	<p>\r\n		Kartu RFID merupakan kartu yang didalamnya terdapat sebuah teknologi chip RFID. RFID sendiri memiliki singkatan dari Radio Frequency Identifiaction yang berfungsi mengirimkan informasi ringkas menggunakan gelombang eletkromagnetik.\r\n	</p>\r\n	<h2>Fungsi Kartu RFID ?</h2>\r\n	<p>\r\n		Kartu RFID memiliki berbagai fungsi, diantaranya :\r\n	</p>\r\n	<h3>Sebagai Alat Pembayaran</h3>\r\n	<p>\r\n		Kehidupan era digitalsisasi selalu berinovasi membuat kemudahan bagi manusia menggunakan teknologi. Dan sekarang metode pembayaran berkembang menjadi cashless. Dengan RFID anda bisa membayar dengan mudah seperti e-money, flash, dll.\r\n	</p>\r\n	<h3>Sebagai Alat Absensi Pada Organisasi</h3>\r\n	<p>\r\n		Kartu RFID menyimpan data pribadi pengguna didalamnya, inilah yang menjadikan RFID dapat digunakan sebagai media absensi. Misal dalam sebuah instansi, menginginkan kemudahan dalam mengola data absensi.\r\n	</p>\r\n	<h2>Berapa Harga Cetak Kartu RFID ?</h2>\r\n	<p>Cetak kartu RFID yang ada inginkan di Umah Multi dengan harga terjangkau dan kualitas terjamin.</p>\r\n	<p>Kami melayani Jasa cetak kartu RFID untuk wilayah Jakarta, Depok, Tangerang, & Bekas ( Jadetabek ) dengan biaya kirim . Kami juga menyediakan jasa desain kartu nama jika anda belum memiliki desain.</p>\r\n	<p>\r\n		Anda berminat ? silahkan klik di sini(Link) untuk beralih ke halaman order atau hubungi kami melalui Whatsapp(Link)\r\n	</p>\r\n</div>','Kartu RFID merupakan kartu yang didalamnya terdapat sebuah teknologi chip RFID. RFID sendiri memiliki singkatan dari Radio Frequency Identifiaction','Kartu RFID,Cetak Kartu RFID,Pengertian Kartu RFID,Kartu RFID Adalah,Fungsi Kartu RFID','intro.jpg','umah-multi','umahmulti-big-logo.png','umah-multi','KzVjaGVITnVGNlU1UkNUanVRemlYdz09',NULL,'2021-02-06 17:06:00',NULL,NULL,NULL,NULL,'Y'),
(3,'TzExanRUZzBRZkRrSU9FbThwL2NxUT09','Kartu Nama','<div class=\"text-justify\">\r\n	<h2>Pengertian, Fungsi, Jenis Material dan Harga Cetak Kartu Nama</h2>\r\n	<p>\r\n		Kartu Nama ? Apa itu kartu nama ? dalam kehidupan sehari-hari pasti sering mendengar kata tersebut dan sudah sering digunakan di sekitar kita. Dalam kehidupan sehari-hari mungkin biasa saja, namun bagi organisasi kartu nama merupakan hal utama dalam membangun hubungan dengan pelanggannya.\r\n	</p>\r\n	<p>\r\n		Dengan adanya kartu nama dapat membantu organisasi dalam menjalin relasi dengan pelanggan. Setiap organisasi dalam memasarkan produknya pasti mereka memberikan kartu nama kepada pelanggannya.\r\n	</p>\r\n	<p>\r\n		Karena kartu nama dapat memberikan nilai lebih dimata pelanggan tentang anda dan organisasi anda. Sehingga pelanggan dapat menilai profesionalitas anda sebagai anggota dari sebuah organisasi.\r\n	</p>\r\n	<h2>Pengertian Kartu Nama</h2>\r\n	<p>\r\n		Kartu nama merupakan sebuah kartu yang berisikan data ringkas mengenai identitas orang pribadi yang difungsikan sebagai perkenalan diri kepada individu lainnya. Biasanya, Kartu nama dibutuhkan dalam bergorganisasi untuk memperkenalkan anggotanya kepada publik. Terutama untuk bidang bisnis, kartu nama dapat memberikan dampak positif bagi organisasi terhadap pelanggan. Karena dalam berorganisasi sudah pasti anggotanya harus memiliki hubungan yang baik dengan pelanggan. Untuk itu, kartu nama dapat membantu pelanggan mengenal anggota organisasi lebih baik.\r\n	</p>\r\n	<p>\r\n		Dalam kartu nama biasanya berisikan detail identitas anggota organisasi, mulai dari Logo organisasi, nama anggota, alamat perusahaan, nomor telepon, email, alamat website, dan lainnya.\r\n	</p>\r\n	<h2>Fungsi Kartu Nama</h2>\r\n	<p>Kartu nama memiliki fungsi penting yang antara lain sebagai berikut :</p>\r\n	<h3>Tanda Pengenal Diri</h3>\r\n	<p>Fungsi utama kartu nama adalah sebagai perkenalan diri tentang identitas dirinya kepada orang lain yang dituliskan didalamnya.</p>\r\n	<h3>Sebagai Personal Branding</h3>\r\n	<p>\r\n		Dalam dunia bisnis seseorang pasti memiliki nilai professionalitas yang berbeda-beda. Dengan kartu nama, dapat membantu meningkatkan nilai professionalitas anda tergantung asumsi dari orang lain terhadap diri anda yang pastinya adalah bernilai positif.\r\n	</p>\r\n	<h3>Perkenalan Organisasi</h3>\r\n	<p>\r\n		Bukan hanya memuat informasi individu saja ! kartu nama juga dapat diisi dengan informasi perusahaan agar lebih dikenal oleh pelanggannya. Mulai dari jenis usaha, alamat, kontak, dsb. Ini dapat meningkatkan citra perusahaan dimata pelanggan.\r\n	</p>\r\n	<h3>Sebagai Alat Promosi dan Pemasaran</h3>\r\n	<p>\r\n		Setiap anggota organisasi pasti memerlukan kartu nama untuk menjalin hubungan baik dengan pelanggannya. Dengan begitu, dapat membantu proses pemasaran menjadi lebih efisien.\r\n	</p>\r\n	<h2>Berapa Harga Cetak Kartu Nama ?</h2>\r\n	<p>Cetak kartu nama yang ada inginkan di Umah Multi dengan harga terjangkau dan kualitas terjamin.</p>\r\n	<p>\r\n		Kami melayani Jasa cetak kartu nama untuk wilayah Jakarta, Depok, Tangerang, & Bekas ( Jadetabek ) dengan biaya kirim . Kami juga menyediakan jasa desain kartu nama jika anda belum memiliki desain.\r\n	</p>\r\n	<p>\r\n		Anda berminat ? silahkan klik di sini(Link) untuk beralih ke halaman order atau hubungi kami melalui Whatsapp(Link).\r\n	</p>\r\n</div>','Kartu nama merupakan sebuah kartu yang berisikan data ringkas mengenai identitas orang pribadi yang difungsikan sebagai perkenalan diri','Kartu Nama,Cetak Kartu Nama,Pengertian Kartu Nama,Kartu Nama Adalah,Fungsi Kartu Nama','intro.jpg','umah-multi','umahmulti-big-logo.png','umah-multi','KzVjaGVITnVGNlU1UkNUanVRemlYdz09',NULL,'2021-02-01 17:06:00',NULL,NULL,NULL,NULL,'Y'),
(1,'U0sybzB3cVlvOVU2dTRmeTRCZGE3R0lDU1N2NHp1anFrM0ZzcWFYUS84TT0','Harga Kartu Nama','<div class=\"text-justify\">\r\n	<h2>Pengertian, Fungsi, Jenis Material dan Harga Cetak Kartu Nama</h2>\r\n	<p>\r\n		Kartu Nama ? Apa itu kartu nama ? dalam kehidupan sehari-hari pasti sering mendengar kata tersebut dan sudah sering digunakan di sekitar kita. Dalam kehidupan sehari-hari mungkin biasa saja, namun bagi organisasi kartu nama merupakan hal utama dalam membangun hubungan dengan pelanggannya.\r\n	</p>\r\n	<p>\r\n		Dengan adanya kartu nama dapat membantu organisasi dalam menjalin relasi dengan pelanggan. Setiap organisasi dalam memasarkan produknya pasti mereka memberikan kartu nama kepada pelanggannya.\r\n	</p>\r\n	<p>\r\n		Karena kartu nama dapat memberikan nilai lebih dimata pelanggan tentang anda dan organisasi anda. Sehingga pelanggan dapat menilai profesionalitas anda sebagai anggota dari sebuah organisasi.\r\n	</p>\r\n	<h2>Pengertian Kartu Nama</h2>\r\n	<p>\r\n		Kartu nama merupakan sebuah kartu yang berisikan data ringkas mengenai identitas orang pribadi yang difungsikan sebagai perkenalan diri kepada individu lainnya. Biasanya, Kartu nama dibutuhkan dalam bergorganisasi untuk memperkenalkan anggotanya kepada publik. Terutama untuk bidang bisnis, kartu nama dapat memberikan dampak positif bagi organisasi terhadap pelanggan. Karena dalam berorganisasi sudah pasti anggotanya harus memiliki hubungan yang baik dengan pelanggan. Untuk itu, kartu nama dapat membantu pelanggan mengenal anggota organisasi lebih baik.\r\n	</p>\r\n	<p>\r\n		Dalam kartu nama biasanya berisikan detail identitas anggota organisasi, mulai dari Logo organisasi, nama anggota, alamat perusahaan, nomor telepon, email, alamat website, dan lainnya.\r\n	</p>\r\n	<h2>Fungsi Kartu Nama</h2>\r\n	<p>Kartu nama memiliki fungsi penting yang antara lain sebagai berikut :</p>\r\n	<h3>Tanda Pengenal Diri</h3>\r\n	<p>Fungsi utama kartu nama adalah sebagai perkenalan diri tentang identitas dirinya kepada orang lain yang dituliskan didalamnya.</p>\r\n	<h3>Sebagai Personal Branding</h3>\r\n	<p>\r\n		Dalam dunia bisnis seseorang pasti memiliki nilai professionalitas yang berbeda-beda. Dengan kartu nama, dapat membantu meningkatkan nilai professionalitas anda tergantung asumsi dari orang lain terhadap diri anda yang pastinya adalah bernilai positif.\r\n	</p>\r\n	<h3>Perkenalan Organisasi</h3>\r\n	<p>\r\n		Bukan hanya memuat informasi individu saja ! kartu nama juga dapat diisi dengan informasi perusahaan agar lebih dikenal oleh pelanggannya. Mulai dari jenis usaha, alamat, kontak, dsb. Ini dapat meningkatkan citra perusahaan dimata pelanggan.\r\n	</p>\r\n	<h3>Sebagai Alat Promosi dan Pemasaran</h3>\r\n	<p>\r\n		Setiap anggota organisasi pasti memerlukan kartu nama untuk menjalin hubungan baik dengan pelanggannya. Dengan begitu, dapat membantu proses pemasaran menjadi lebih efisien.\r\n	</p>\r\n	<h2>Berapa Harga Cetak Kartu Nama ?</h2>\r\n	<p>Cetak kartu nama yang ada inginkan di Umah Multi dengan harga terjangkau dan kualitas terjamin.</p>\r\n	<p>\r\n		Kami melayani Jasa cetak kartu nama untuk wilayah Jakarta, Depok, Tangerang, & Bekas ( Jadetabek ) dengan biaya kirim . Kami juga menyediakan jasa desain kartu nama jika anda belum memiliki desain.\r\n	</p>\r\n	<p>\r\n		Anda berminat ? silahkan klik di sini(Link) untuk beralih ke halaman order atau hubungi kami melalui Whatsapp(Link).\r\n	</p>\r\n</div>','Kartu nama merupakan sebuah kartu yang berisikan data ringkas mengenai identitas orang pribadi yang difungsikan sebagai perkenalan diri','Kartu Nama,Cetak Kartu Nama,Harga Kartu Nama','intro.jpg','umah-multi','umahmulti-big-logo.png','umah-multi','KzVjaGVITnVGNlU1UkNUanVRemlYdz09',NULL,'2021-02-01 17:06:00',NULL,NULL,NULL,NULL,'Y'),
(4,'U0sybzB3cVlvOVU2dTRmeTRCZGE3TTBlZXNGRURlK1hhSzBsa3M4bnZxUT0','Harga Kartu RFID','<div class=\"text-justify\">\r\n	<h2>Pengertian, Fungsi, Jenis Material dan Harga Cetak Kartu RFID</h2>\r\n	<p>\r\n		Apakah anda memahami, apa itu Kartu RFID ? Apakah anda pernah melihat dan menggunakannya ? mungkin saja anda menggunakan Kartu RFID namun tidak menyangka kartu yang anda gunakan adalah Kartu RFID. Dalam kehidupan sehari-hari misalnya, seperti kartu e-money, flazz dan sebagainya.\r\n	</p>\r\n	<h2>Pengertian Kartu RFID ?</h2>\r\n	<p>\r\n		Kartu RFID merupakan kartu yang didalamnya terdapat sebuah teknologi chip RFID. RFID sendiri memiliki singkatan dari Radio Frequency Identifiaction yang berfungsi mengirimkan informasi ringkas menggunakan gelombang eletkromagnetik.\r\n	</p>\r\n	<h2>Fungsi Kartu RFID ?</h2>\r\n	<p>\r\n		Kartu RFID memiliki berbagai fungsi, diantaranya :\r\n	</p>\r\n	<h3>Sebagai Alat Pembayaran</h3>\r\n	<p>\r\n		Kehidupan era digitalsisasi selalu berinovasi membuat kemudahan bagi manusia menggunakan teknologi. Dan sekarang metode pembayaran berkembang menjadi cashless. Dengan RFID anda bisa membayar dengan mudah seperti e-money, flash, dll.\r\n	</p>\r\n	<h3>Sebagai Alat Absensi Pada Organisasi</h3>\r\n	<p>\r\n		Kartu RFID menyimpan data pribadi pengguna didalamnya, inilah yang menjadikan RFID dapat digunakan sebagai media absensi. Misal dalam sebuah instansi, menginginkan kemudahan dalam mengola data absensi.\r\n	</p>\r\n	<h2>Berapa Harga Cetak Kartu RFID ?</h2>\r\n	<p>Cetak kartu RFID yang ada inginkan di Umah Multi dengan harga terjangkau dan kualitas terjamin.</p>\r\n	<p>Kami melayani Jasa cetak kartu RFID untuk wilayah Jakarta, Depok, Tangerang, & Bekas ( Jadetabek ) dengan biaya kirim . Kami juga menyediakan jasa desain kartu nama jika anda belum memiliki desain.</p>\r\n	<p>\r\n		Anda berminat ? silahkan klik di sini(Link) untuk beralih ke halaman order atau hubungi kami melalui Whatsapp(Link)\r\n	</p>\r\n</div>','Kartu RFID merupakan kartu yang didalamnya terdapat sebuah teknologi chip RFID. RFID sendiri memiliki singkatan dari Radio Frequency Identifiaction','Kartu RFID,Cetak Kartu RFID,Harga Kartu RFID','intro.jpg','umah-multi','umahmulti-big-logo.png','umah-multi','KzVjaGVITnVGNlU1UkNUanVRemlYdz09',NULL,'2021-02-06 17:06:00',NULL,NULL,NULL,NULL,'Y');

/*Table structure for table `umh_m9l2t0_category` */

DROP TABLE IF EXISTS `umh_m9l2t0_category`;

CREATE TABLE `umh_m9l2t0_category` (
  `id_category` varchar(200) NOT NULL,
  `cat_name` varchar(50) DEFAULT NULL,
  `cat_desc` varchar(500) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_category` */

insert  into `umh_m9l2t0_category`(`id_category`,`cat_name`,`cat_desc`,`created_by`,`created_date`,`created_ip`,`modified_by`,`modified_date`,`modified_ip`,`is_active`) values 
('NGhhdzNCMDhpZlFpSld3NUxXQVpmZz09','Jasa Desain & Web',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('T1RNWXZxNTdnWXFuLzMyWEZnR2JMNXhFZUVzQUZIT3BScEZwN3RpK2hVRT0','Jasa Print Online',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y');

/*Table structure for table `umh_m9l2t0_cust_review` */

DROP TABLE IF EXISTS `umh_m9l2t0_cust_review`;

CREATE TABLE `umh_m9l2t0_cust_review` (
  `id_cr` varchar(200) NOT NULL,
  `cr_name` varchar(50) DEFAULT NULL,
  `cr_desc` varchar(200) DEFAULT NULL,
  `cr_img` varchar(150) DEFAULT NULL,
  `cr_img_alt` varchar(100) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_cr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_cust_review` */

insert  into `umh_m9l2t0_cust_review`(`id_cr`,`cr_name`,`cr_desc`,`cr_img`,`cr_img_alt`,`created_by`,`created_date`,`created_ip`,`modified_by`,`modified_date`,`modified_ip`,`is_active`) values 
('K0EySE9UN2VzdGRQeG1ramtKYzZldz09','cr-06','This card has supporting text below as a natural lead-in to additional content.','umahmulti.png','umah-multi',NULL,'2021-02-01 17:06:00',NULL,NULL,NULL,NULL,'Y'),
('M3FKelZjUno3Zmp0KzgzRU1Na2R6UT09','cr-03','This card has supporting text below as a natural lead-in to additional content.','umahmulti.png','umah-multi',NULL,'2021-02-01 13:00:00',NULL,NULL,NULL,NULL,'Y'),
('Mm9zT2o5Zm50dUIxREY3cDdyZktCdz09','cr-02','This card has supporting text below as a natural lead-in to additional content.','umahmulti.png','umah-multi',NULL,'2021-02-01 07:00:00',NULL,NULL,NULL,NULL,'Y'),
('QkJYa1didG4yTm5LS3k1RS9Ed1BQUT09','cr-01','This card has supporting text below as a natural lead-in to additional content.','intro.jpg','umah-multi',NULL,'2021-02-01 07:00:00',NULL,NULL,NULL,NULL,'Y'),
('RzVieUJvcW9kOGRONnVWWFdHUzNvQT09','cr-05','This card has supporting text below as a natural lead-in to additional content.','intro.jpg','umah-multi',NULL,'2021-02-01 17:00:00',NULL,NULL,NULL,NULL,'Y'),
('YjRtVEgrSkRTb0ZjSXVWR2tjRmE3dz09','cr-04','This card has supporting text below as a natural lead-in to additional content.','intro.jpg','umah-multi',NULL,'2021-02-01 14:00:00',NULL,NULL,NULL,NULL,'Y');

/*Table structure for table `umh_m9l2t0_custom_field` */

DROP TABLE IF EXISTS `umh_m9l2t0_custom_field`;

CREATE TABLE `umh_m9l2t0_custom_field` (
  `id_custom_field` varchar(10) NOT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `value` varchar(500) DEFAULT NULL,
  `field_order` int(10) DEFAULT NULL,
  `m9l2t0_product_id` varchar(100) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_custom_field`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_custom_field` */

insert  into `umh_m9l2t0_custom_field`(`id_custom_field`,`field_name`,`type`,`value`,`field_order`,`m9l2t0_product_id`,`created_by`,`created_date`,`created_ip`,`modified_by`,`modified_date`,`modified_ip`,`is_active`) values 
('0zIkUMB8xY','Material','dropdown','Art Paper 120 gr,Art Paper 150 gr,HVS,Mate Paper 120 gr,Mate Paper 150 gr',NULL,'14',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('27rBu3Q0zc','Jumlah Halaman','text',NULL,7,'12,13,18',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('2Ggdb7ExN4','Material','dropdown','Art Carton 230 gr,Art Carton 260 gr,Art Carton 310 gr,Linen,Satin,Yupo',1,'8',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('5G3VAOy7iI','Ukuran','dropdown','60x160 cm,80x180 cm\r\n',NULL,'3',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('8k5CBSPzNp','Laminasi','dropdown','Glossy Cover,Glossy All,Doff Cover,Doff All\r\n',NULL,'12,18',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('9sfBAqYc0b','Model','dropdown','Jilid steples,Jilid Jahit Benang,Jilid Spiral',NULL,'12,13,18',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('a7QnzhiKYA','Material','dropdown','Albatros',NULL,'10',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('ar6iyhb7GJ','Ukuran Stiker','text',NULL,NULL,'17',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('AVSpmhk4Rt','Material Cover','dropdown','Art Carton 190 gr,Art Carton 210 gr,Art Carton 230 gr,Art Carton 260 gr,Art Carton 310 gr,Art Carton 350 gr,Art Paper 120 gr,Art Paper 150 gr,HVS',2,'12',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('bKGg06WQEy','Model','dropdown','Alumunium,Stainless\r\n',NULL,'2',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('BqA42VUW9n','Model','dropdown','Lipat 2,Tidak Lipat\r\n',NULL,'11,25',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('bSGu7LHmD2','Material','dropdown','HVS',NULL,'13',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('CLg0YiDB3R','Model','dropdown','Lipat 2,Lipat 3,Lipat 4,Tidak lipat\r\n',NULL,'14',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('edyOKBt62Q','Ukuran','dropdown','A3,A4,A5,A6,1/3 A4\r\n',NULL,'14',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('Emu2gcM9ki','Cutting','dropdown','Sudut Lancip,Sudut Melengkung\r\n',NULL,'8',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('fJFln6WdiX','Ukuran','dropdown','A4,A5,A6\r\n',NULL,'12,18,16,24,11,25',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('g5Lka3xm4h','Model','dropdown','2 sisi,1 sisi\r\n',NULL,'8,27',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('glHOs6jMir','Model','dropdown','Jilid Spiral Atas,Jilid Spiral Samping,Jilid Lem / Lakban Samping',NULL,'4,7',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('gM4s5KP9ty','Laminasi','dropdown','Glossy,Doff',NULL,'2,3,5,8,10,14,16,17,20,22,23,24,27',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('GoChlFpudB','Material','dropdown','Art Carton 210 gr,Art Paper 120 gr,Art Paper 150 gr,Mate Paper 120 gr,Mate Paper 150 gr',NULL,'24',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('I3XnABwdRF','Material','dropdown','Art Carton 210 gr,Art Paper 120 gr,Art Paper 150 gr,Mate Paper 120 gr,Mate Paper 150 gr',NULL,'16',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('JFIZHh8EOi','Model','dropdown','Spiral Atas (Dinding),Jepit Kaleng (Dinding),Spiral Atas (Meja)\r\n',NULL,'22',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('jmLegkhAyR','Ukuran','dropdown','60x160 cm,80x180 cm,25x40cm\r\n',NULL,'23',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('kgrlnPm2zA','Ukuran Bahan Stiker','text',NULL,NULL,'17',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('Klhey9UBQr','Material Isi','dropdown','Art Carton 210 gr,Art Carton 230 gr,Art Carton 260 gr,Art Paper 100 gr,HVS',NULL,'18',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('l23Jv6A9kR','Satuan Ukuran Stiker','dropdown','MM,CM,M',NULL,'17',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('NdDVCmqicL','Ukuran','dropdown','120x200 cm,150x200 cm,60x160 cm,85x200cm\r\n',NULL,'2',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('p61v0GnTL3','Ukuran','dropdown','A3 (Dinding),B2 (Dinding),A5 (Kalender Meja),A6 (Kalender Meja)\r\n',NULL,'22',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('rU0m1ZEp4l','Jenis Cetak','dropdown','Digital,Offset',6,'10,12,14,16,18,22,24',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('sOaHDdASyw','Material','dropdown','Flexi Jerman,Flexi Korea,Flexi China,Albatros',NULL,'5,2,23,3,20',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('T6VHR9JtwU','Material','dropdown','Art Carton 230 gr,Art Carton 260 gr',NULL,'11,22,25',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('TeAydkg4a9','Model','dropdown','Stempel Flash,Stempel Runaflex\r\n',NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('tJwNcFS5oD','Material Cover','dropdown','Art Carton 210 gr,Art Carton 230 gr,Art Carton 260 gr,Art Paper 100 gr,HVS',NULL,'18',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('TN8kAg0Zoj','Ukuran','dropdown','A4,Letter\r\n,Legal (F4)',NULL,'13',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('umxp3q9cTQ','Ukuran','text','Custom',4,'4,5,10,7,20',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('V4XISkEWuD','Material Isi','dropdown','Art Carton 190 gr,Art Carton 210 gr,Art Carton 230 gr,Art Carton 260 gr,Art Carton 310 gr,Art Carton 350 gr,Art Paper 120 gr,Art Paper 150 gr,HVS\r\n',NULL,'12',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('Yw3GBR7ZcM','Ukuran','dropdown','Persegi 28 x 10 mm,Persegi 43 x 17 mm,Persegi 43 x 22 mm,Persegi 43 x 27 mm,Persegi 55 x 13 m,Persegi 55 x 17 mm, Persegi 55 x 22 mm,Persegi 55 x 32 mm,Persegi 55 x 43 mm, Persegi 33  13 mm,Persegi 67 x 22 mm,Persegi 67 x 27 mm,Persegi 67 x 32 mm,Persegi 78 x 27 mm,Persegi 78 x 32 mm,Persegi 78 x 53 mm,Persegi 43 x 13 mm,Persegi 55 x 17 mm, Persegi 25 x 5 m,Persegi 30 x 30 mm,Persegi 35 x 35 mm,Bulat Diameter 17 mm,Bulat Diameter 23 mm,Bulat Diameter 28 mm,Bulat Diameter 35 mm,Bulat Diameter 40 ',NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('ZJOul7FbTs','Cutting','dropdown','Iya,Tidak',NULL,'17',NULL,NULL,NULL,NULL,NULL,NULL,'Y');

/*Table structure for table `umh_m9l2t0_customer` */

DROP TABLE IF EXISTS `umh_m9l2t0_customer`;

CREATE TABLE `umh_m9l2t0_customer` (
  `id_cust` varchar(10) NOT NULL,
  `cust_firstname` varchar(50) DEFAULT NULL,
  `cust_lastname` varchar(50) DEFAULT NULL,
  `cust_email` varchar(10) DEFAULT NULL,
  `cust_phone` text DEFAULT NULL,
  `cust_address` varchar(500) DEFAULT NULL,
  `state_id` varchar(10) DEFAULT NULL,
  `cust_id` varchar(5) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_cust`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_customer` */

/*Table structure for table `umh_m9l2t0_faq` */

DROP TABLE IF EXISTS `umh_m9l2t0_faq`;

CREATE TABLE `umh_m9l2t0_faq` (
  `id_faq` varchar(10) NOT NULL,
  `faq_ask` varchar(50) DEFAULT NULL,
  `faq_desc` text DEFAULT NULL,
  `faq_order` int(2) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_faq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_faq` */

insert  into `umh_m9l2t0_faq`(`id_faq`,`faq_ask`,`faq_desc`,`faq_order`,`created_by`,`created_date`,`created_ip`,`modified_by`,`modified_date`,`modified_ip`,`is_active`) values 
('EsRpnm1wXa','Jasa Website','<h5>Apa itu Website ?</h5><p>Website merupakan media informasi yang dapat diakses semua orang. Untuk memasarkan produk website memiliki keunggulan sebagai citra organisasi bagi pelanggannya. Karena website merupakan identitas sebuah organisasi di dunia intetnet.</p><h5>Mengapa harus website ?</h5><p>Untuk dapat bersaing pada era industry 4.0, mempunyai website adalah hal yang wajib bagi para pebisnis karena semua teknologi sekarang membutuhkan akses cepat dan memudahkan pelanggannya dalam bertransaksi.</p><h5>Apa saja yang harus dipersiapkan ?</h5><p>Hal yang utama perlu dipersiapkan adalah konten atau artikel yang akan di publish pada website dan berbagai macam gambar pendukung.</p><h5>Bagaimana mengelola website sedangkan saya pemula ?</h5><p>Tenang saja Umah Multi memberikan kemudahan pelayanan bagi pelanggannya. Kami menyediakan berbagai macam template yang siap pakai dan dapat di sesuaikan menggunakan CMS (Content Manajemen System )yang sudah disiapkan oleh kami. Jadi anda hanya perlu menyiapkan konten saja. Untuk kesulitan lainnya dapat menghubungi kami melalui whatsapp atau email.</p><h5>Apakah website menerapkan SEO ?</h5><p>Umah multi menerapkan teknik White Hat SEO pada CMS-nya, apa sih white hat SEO itu ? anda bisa membacanya di sini.</p>',3,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('mtF6fe2hj3','Jasa Desain Grafis','<h5>Bagaimana cara order desain grafis ?</h5><ul><li>Untuk jasa desain grafis dapat dilakukan dengan melakukan pengisian form terlebih dahulu yang ada pada website.</li><li>Kemudian Tim kami akan menghubungi pelanggan ke whatsapp anda secara langsung.</li></ul><h5>Berapa Harga dari desain grafis yang saya inginkan ?</h5><ul><li>Untuk harga desain grafis tergantung dari tingkat kesulitan yang ada.</li><li>Harga didiskusikan langsung oleh tim kami pada layanan whatsapp.</li></ul><h5>Bagaimana cara melakukan pembayaran</h5><ul><li>Pembayaran order desain grafis hanya dapat dilakukan dengan menggunakan Bank Transfer dan hanya pada no. rekening berikut :</li></ul><h5>Bagaimana saya menerima file desain yang sudah jadi ?</h5><ul><li>File final yang dikirim akan diberikan dalam bentuk zip, berisikan beberapa format yakni AI / PSD (tergantung jenis desain),PDF, JPG dan TIFF kepada email pelanggan.</li></ul>',2,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('wMpfqn1Nl4','Jasa Percetakan','<h5>File seperti apa yang dapat diterima ?</h5><ul><li>File memiliki resolusi minimal 300dpi.</li><li>Jenis file yang diperbolehkan adalah dengan format PDF, AI, PSD, JPEG, PNG, dan TIFF. Namun kami anjurkan menggunakan format PDF untuk file desain vector.</li><li>Maksimal ukuran file yang diperbolehkan oleh kami adalah 500MB. Atau lebih dari itu bisa menggunakan penyimpanan cloud.</li><li>Untuk media penyimpanan cloud kami sarankan menggunakan google drive atau dropbox, bisa juga dikirim ke email kami @umahmulti.biz.id menggunakan WeTransfer.</li><li>Untuk akurasi warna kami sarankan menggunakan mode warna CMYK, karena mesin cetak menggunakan mode warna tersebut.</li></ul><h5>Bagaimana dan Berapa Lama Proses Ordernya ?</h5><ul><li>Untuk memudahkan proses pemesanan, kami akan melakukannya dengan komunikasi whatsapp setelah pelanggan mengisi form yang diperlukan.</li><li>Lama waktu proses kami adalah paling lambat 3 hari, kami menjamin ketepatan waktu untuk memberikan pelayanan prima kepada pelanggan.</li><li>Saat melakukan order, Mohon pastikan kembali detail spesifikasi dan file desain anda, apakah sudah sesuai dengan yang diinginkan.</li><li>Kesalahan data desain atau spesifikasi bukan tanggung jawab kami.</li><li>Pengiriman barang akan dilakukan oleh tim kami dan berlangsung selama 1x24jam dari konfirmasi pengiriman yang diinfokan melalui email dan Whatsapp.</li></ul><h5>Bagaimana cara pembayarannya ?</h5><ul><li>Pesanan dengan nilai dibawah 100000, dilakukan pembayaran full.</li><li>Pesanan dengan nilai diatas 100000, dilakukan dengan Down Payment 50% menggunakan metode pembayaran yang tersedia. Dan untuk pelunasan dapat dilakukan ketika pesanan sampai.</li><li>Metode pembayaran yang kami terima adalah melalui Bank Transfer Mandiri & BCA.</li><li>Lama waktu proses verifikasi pembayaran dibutuhkan beberapa menit (maksimal 10 menit).</li><li>Jika terjadi kendala pada saat setelah melakukan pembayaran, anda dapat menghubungi kami melalui whatsapp.</li></ul><h5>Jika Saya Ingin Membatalkan Pesanan Namun sudah melakukan pembayaran ?</h5><ul><li>Mohon maaf untuk pembatalan pemesanan tidak dapat dilakukan ketika proses pemesanan telah diselesaikan.</li></ul><h5>Jika Saya Ingin Mencetak Namun Tidak Punya Desain ?</h5><ul><li>Untuk pelanggan yang ingin mencetak namun belum memiliki desain, kami menyediakan jasa desain dengan harga yang disesuaikan dengan tingkat kesulitan desain tersebut.</li></ul><h5>Saya Tidak Mengerti Tentang Spesifikasi yang dibutuhkan ?</h5><ul><li>Detail spesifikasi yang dibutuhkan ketika proses order dapat dipelajari pada halaman artikel Umah Multi.</li></ul>',1,NULL,NULL,NULL,NULL,NULL,NULL,'Y');

/*Table structure for table `umh_m9l2t0_image` */

DROP TABLE IF EXISTS `umh_m9l2t0_image`;

CREATE TABLE `umh_m9l2t0_image` (
  `id_img` varchar(10) NOT NULL,
  `img_name` varchar(150) DEFAULT NULL,
  `img_desc` text DEFAULT NULL,
  `img_loc` char(1) DEFAULT NULL COMMENT '1:home 2:ilustrasi prod',
  `parent` int(11) DEFAULT NULL,
  `img_alt` varchar(100) DEFAULT NULL,
  `img_order` int(2) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_img`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_image` */

insert  into `umh_m9l2t0_image`(`id_img`,`img_name`,`img_desc`,`img_loc`,`parent`,`img_alt`,`img_order`,`created_by`,`created_date`,`created_ip`,`modified_by`,`modified_date`,`modified_ip`,`is_active`) values 
('blcxTHFSal','Slide-2.png',NULL,'1',NULL,'umah-multi-02',2,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('lEk9JPRowf','kartu-nama.png',NULL,'2',8,'kartu-nama',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('Q09MbnloL0','Slide-3.png',NULL,'1',NULL,'umah-multi-03',3,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('UmZhUXZ6QU','Slide-1.png',NULL,'1',NULL,'umah-multi',1,NULL,NULL,NULL,NULL,NULL,NULL,'Y');

/*Table structure for table `umh_m9l2t0_laminating` */

DROP TABLE IF EXISTS `umh_m9l2t0_laminating`;

CREATE TABLE `umh_m9l2t0_laminating` (
  `id_laminating` varchar(10) NOT NULL,
  `laminate_name` varchar(50) DEFAULT NULL,
  `laminate_desc` text DEFAULT NULL,
  `product_id` varchar(10) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_laminating`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_laminating` */

/*Table structure for table `umh_m9l2t0_level` */

DROP TABLE IF EXISTS `umh_m9l2t0_level`;

CREATE TABLE `umh_m9l2t0_level` (
  `id_level` varchar(10) NOT NULL,
  `lvl_name` varchar(50) NOT NULL,
  `lvl_desc` varchar(100) DEFAULT NULL,
  `menu_access` varchar(100) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_level` */

insert  into `umh_m9l2t0_level`(`id_level`,`lvl_name`,`lvl_desc`,`menu_access`,`created_by`,`created_date`,`created_ip`,`modified_by`,`modified_date`,`modified_ip`,`is_active`) values 
('9898989898','Super Admin','super admin','1,2,3,4,5,6',NULL,NULL,NULL,NULL,NULL,NULL,'Y');

/*Table structure for table `umh_m9l2t0_material` */

DROP TABLE IF EXISTS `umh_m9l2t0_material`;

CREATE TABLE `umh_m9l2t0_material` (
  `id_material` varchar(10) NOT NULL,
  `material_name` varchar(50) DEFAULT NULL,
  `material_desc` text DEFAULT NULL,
  `product_id` varchar(10) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_material`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_material` */

/*Table structure for table `umh_m9l2t0_menu` */

DROP TABLE IF EXISTS `umh_m9l2t0_menu`;

CREATE TABLE `umh_m9l2t0_menu` (
  `id_menu` varchar(200) NOT NULL,
  `menu_name` varchar(50) DEFAULT NULL,
  `menu_icon` varchar(150) DEFAULT NULL,
  `menu_link` varchar(100) DEFAULT NULL,
  `menu_level` tinyint(2) DEFAULT NULL,
  `menu_order` tinyint(2) DEFAULT NULL,
  `menu_asparent` tinyint(4) DEFAULT NULL,
  `menu_parent` varchar(200) DEFAULT NULL,
  `menu_loc` char(1) DEFAULT '1' COMMENT '1:user 2:admin',
  `desc_menu` varchar(500) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_menu` */

insert  into `umh_m9l2t0_menu`(`id_menu`,`menu_name`,`menu_icon`,`menu_link`,`menu_level`,`menu_order`,`menu_asparent`,`menu_parent`,`menu_loc`,`desc_menu`,`created_by`,`created_date`,`created_ip`,`modified_by`,`modified_date`,`modified_ip`,`is_active`) values 
('amRGUmtOQnZ2OGg3TlVJRHRoMkh6UT09','Paper','fa-newspaper-o',NULL,2,2,NULL,'RUovbDkxbXNidTNNaGdxV0V4UURMOEl4S0tMV0NZZzdveGlsZDNENnJLaz0','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('c1pWck96bUtYbEozUi9KTG9xSUE3Zz09','Cetakan Besar','fa-expand',NULL,NULL,3,NULL,'WFlrQXIrTG83cWR0bkEyUzh0YmNwR093Vk11MEZmcjFQTGU3Sk16Q3BSbz0','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('dzdIYjEvaGRCZTBXNUU1UUo0WExVdz09','Activity',NULL,NULL,1,3,1,NULL,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('dzlVMUdWUHViVCs0b1NBZ0R5K3k1UT09','Surat Resmi','fa-file-text-o',NULL,2,2,NULL,'S3JuMU9OeHJjdlJRQkIyTXJVLzNyTGNkTUZrV2RVWW5VaUlwb1Yya0J5Yz0','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('K0lia0dnWG1kOUNpOHV6RGt5NkVJdz09','Dashboard','home','dashboard',1,1,NULL,NULL,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('KzVjaGVITnVGNlU1UkNUanVRemlYdz09','Kartu Identitas','fa-id-card-o',NULL,2,1,NULL,'S3JuMU9OeHJjdlJRQkIyTXJVLzNyTGNkTUZrV2RVWW5VaUlwb1Yya0J5Yz0','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('N3g3SU43Y1hBWHFVZGVRVi9YejlkZz09','Umum','fa-puzzle-piece',NULL,NULL,4,NULL,'WFlrQXIrTG83cWR0bkEyUzh0YmNwR093Vk11MEZmcjFQTGU3Sk16Q3BSbz0','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('Q21xbFU3a1kzS2gzajhmTVlPMVFDZz09','Aksesoris','fa-puzzle-piece',NULL,2,4,NULL,'S3JuMU9OeHJjdlJRQkIyTXJVLzNyTGNkTUZrV2RVWW5VaUlwb1Yya0J5Yz0','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('RUovbDkxbXNidTNNaGdxV0V4UURMOEl4S0tMV0NZZzdveGlsZDNENnJLaz0','Kebutuhan Pribadi',NULL,'produk/kebutuhan-pribadi',1,3,1,NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('RW1iU3lRM3BNeWRjU1UreSt0czlUdz09','Manage',NULL,NULL,1,2,1,NULL,'2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('S0tPR2FjRk5wWjM1Y0h0UWxxNnNSRlp0eTlJdEw4REpGSVNCOUgvMFd1cz0','Jasa Desain & Website',NULL,NULL,1,4,1,NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('S3JuMU9OeHJjdlJRQkIyTXJVLzNyTGNkTUZrV2RVWW5VaUlwb1Yya0J5Yz0','Kebutuhan Organisasi',NULL,'produk/kebutuhan-organisasi',1,1,1,NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('TEQrdmZ0cHl0ajI2M1oyaDBCK3dWdz09','Menu',NULL,'configuration/menu',3,1,NULL,'V252ZHBOVExvZXJtbHptWU0wOHVEZz09','2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('Ty9UamlOYnVjMStobE5JcU83RjVrdz09','Cetakan Pers','fa-book',NULL,2,1,NULL,'WFlrQXIrTG83cWR0bkEyUzh0YmNwR093Vk11MEZmcjFQTGU3Sk16Q3BSbz0','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('UG03SXVnOFhDVGVUdmFMaEc4QUJ4dz09','Selebaran','fa-file-image-o',NULL,NULL,2,NULL,'WFlrQXIrTG83cWR0bkEyUzh0YmNwR093Vk11MEZmcjFQTGU3Sk16Q3BSbz0','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('V252ZHBOVExvZXJtbHptWU0wOHVEZz09','Configuration','settings',NULL,2,1,1,'RW1iU3lRM3BNeWRjU1UreSt0czlUdz09','2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('WFlrQXIrTG83cWR0bkEyUzh0YmNwR093Vk11MEZmcjFQTGU3Sk16Q3BSbz0','Kebutuhan Pemasaran',NULL,'produk/kebutuhan-pemasaran',1,2,1,NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('Y2F6SU1hYTUwUS83Tm5YUElsVmh3UT09','Kartu Undangan','fa-paper-plane-o',NULL,2,1,NULL,'RUovbDkxbXNidTNNaGdxV0V4UURMOEl4S0tMV0NZZzdveGlsZDNENnJLaz0','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('YkQzc1Bmc1NZVVFOVUVkbk5BUFBJdz09','Transaksi','shopping-cart','transaksi',2,1,NULL,'dzdIYjEvaGRCZTBXNUU1UUo0WExVdz09','2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y');

/*Table structure for table `umh_m9l2t0_model` */

DROP TABLE IF EXISTS `umh_m9l2t0_model`;

CREATE TABLE `umh_m9l2t0_model` (
  `id_model` varchar(10) NOT NULL,
  `model_name` varchar(50) DEFAULT NULL,
  `model_desc` text DEFAULT NULL,
  `model_img` varchar(150) DEFAULT NULL,
  `model_img_alt` varchar(100) DEFAULT NULL,
  `product_id` varchar(10) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_model`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_model` */

/*Table structure for table `umh_m9l2t0_our_team` */

DROP TABLE IF EXISTS `umh_m9l2t0_our_team`;

CREATE TABLE `umh_m9l2t0_our_team` (
  `id_ot` varchar(200) NOT NULL,
  `ot_name` varchar(50) DEFAULT NULL,
  `ot_linked` varchar(100) DEFAULT NULL,
  `ot_twitter` varchar(100) DEFAULT NULL,
  `ot_ig` varchar(100) DEFAULT NULL,
  `ot_desc` text DEFAULT NULL,
  `ot_img` varchar(150) DEFAULT NULL,
  `ot_img_alt` varchar(100) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_ot`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_our_team` */

insert  into `umh_m9l2t0_our_team`(`id_ot`,`ot_name`,`ot_linked`,`ot_twitter`,`ot_ig`,`ot_desc`,`ot_img`,`ot_img_alt`,`created_by`,`created_date`,`created_ip`,`modified_by`,`modified_date`,`modified_ip`,`is_active`) values 
('NVBDd3NzeU1ldlJHUGIyZjFTZDNLY2c4R3dkSEx1a1lWWlg1TkEzck1rYz0','Ade Rahmad Kurniawan',NULL,NULL,NULL,'This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.','intro.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('QUMzYkR1c09HU1R3Q3VMNjk4ZzY5dz09','Aji Setiyawan',NULL,NULL,NULL,'This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.','intro.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('Vk1jM2x2SzljMExWendTNVJ1dGpGQT09','M. Eriza Yusup',NULL,NULL,NULL,'This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.','intro.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('WE9LbkpMUEhHTGhSL0dEV09wdEYrNUlDeGlNbUhUWllmdmJmbzZSdjdSMD0','Fefbyanto Saputro',NULL,NULL,NULL,'This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.','intro.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y');

/*Table structure for table `umh_m9l2t0_policy` */

DROP TABLE IF EXISTS `umh_m9l2t0_policy`;

CREATE TABLE `umh_m9l2t0_policy` (
  `id_policy` varchar(200) NOT NULL,
  `content` text DEFAULT NULL,
  `policy_type` char(1) DEFAULT NULL COMMENT '1: tos 2:privasi',
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_policy`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_policy` */

insert  into `umh_m9l2t0_policy`(`id_policy`,`content`,`policy_type`,`created_by`,`created_date`,`created_ip`,`modified_by`,`modified_date`,`modified_ip`,`is_active`) values 
('eGVNZENhKy9QVTc1bGlSSVNiQkljczA0clBnbWw0SHl4T1AyV2RSbElPST0','<p>Dengan membaca ini, Umah Multi sebagai (Kami) pelanggan sebagai (Pelanggan). Dianggap telah memahami dan menerima Syarat dan ketentuan berikut tertulis, demi mewujudkan kenyamanan berbagai pihak. Syarat dan Ketentuan ini dapat berubah sewaktu-waktu tanpa pemberitahuan terlebih dahulu. Jika merasa keberatan dengan perjanjian ini, kami harap anda tidak menggunakan situs kami.</p><h5>Ketentuan Pelangganan Situs</h5><p>Berikut beberapa ketentuan Pelanggan dalam penggunaan situs Umah Multi.</p><ul><li>Pelanggan minimal sudah berusia 18 Tahun dan sudah memiliki persyaratan yang diakui secara hukum.</li><li>Pelanggan yang mengoperasikan situs diperkenankan hanya untuk kepentingan dan keperluan yang berkaitan dengan layanan.</li><li>Pelanggan tidak diperkenankan menggunakan konten yang melanggar hukum serperti Hak Cipta, Pornografi, Virus, dsb.</li><li>Pelanggan harus menggunakan identitas yang dapat dipertanggung jawabkan.</li></ul><h5>Review Pelanggan</h5><p>Berikut beberapa ketentuan Pelanggan dalam memberikan ulasan produk.</p><ul><li>Pelanggan dapat memberikan ulasan dengan bahasa yang sopan dan dapat di pertanggungjawabkan.</li><li>Gambar (Produk) pelanggan dapat kami gunakan untuk kebutuhan website.</li><li>Review Pelanggan dapat kami gunakan untuk di upload pada website dan tidak dapat di kembalikan.</li></ul><h5>Hak Milik Intelektual</h5><p>Berikut beberapa ketentuan Pelanggan dalam menggunakan produk.</p><p>Kami memiliki website berisikan konten yang dilindungi undang-undang hak cipta dan undang-undang melindungi kekayaan intelektual. Seluruh konten pada website ini adalah mutlak hak milik kami. Kami memberikan perizinan penggunaan konten produk Umah Multi hanya untuk Pelanggan kami yang telah melakukan order.</p><h5>Transaksi</h5><p>Berikut beberapa ketentuan Pelanggan dalam proses transaksi.</p><ul><li>Untuk pembelian produk pada website kami, pesanan akan diproses setelah admin telah memverifikasi pembayaran anda.</li><li>Lama proses order tergantung dari jumlah dan jarak tujuan tempat Pelanggan. Untuk pengiriman kami menggunakan tim internal kami.</li><li>Kami tidak memberikan asuransi dan tidak bertanggung tanggung jawab atas kendala pengiriman yang disebabkan diluar kemampuan kami seperti bencana alam, perampokan, dsb.</li><li>Setiap produk yang dibeli Pelanggan dari kami memiliki masa garansi yang berbeda pada masing-masing produk.</li><li>Barang yang telah dibeli tidak dapat dikembalikan atau ditukar barang lainnya. Kecuali ada perjanjian terlebih dahulu.</li><li>Untuk komplain produk akan kami terima maksimal 1x24 jam setelah produk diterima.</li><li>Kami sudah berupaya maksimal untuk memberikan detail data dan spesifikasi produk. Namun kami tidak menjamin keakuratan informasi tersebut.</li></ul><h5>Harga</h5><p>Berikut beberapa ketentuan Pelanggan dalam memahami keadaan nilai dari produk kami.</p><ul><li>Untuk menjaga harga jual yang bersaing dipasaran, kami berusaha untuk menyediakan produk untuk ready stok, namun kami tidak menjamin seluruh produk yang ada pada kami selalu ready stok.</li><li>Setiap produk memiliki harga yang sewaktu-waktu berubah tanpa pemberitahuan kepada Pelanggan.</li><li>Kami berupaya maksimal untuk memberikan harga yang akurat dan minim kesalahan, akan tetapi jika ada kesalahan pada harga seperti typo atau kesalahan harga yang berasal dari internal kami. Kami berhak untuk membatalkan pesanan secara sepihak. Dan apabila pesanan telah di proses dan di bayarkan, kami akan mengembalikan dana sesuai dengan jumlah yang telah di bayarkan.</li><li>Harga yang dikirimkan pada saat proses order akan dirinci secara detail dan termasuk biaya kirim ke tempat tujuan.</li><li>Untuk biaya pengiriman kami memiliki ketentuan yakni Rp. 5000 untuk wilayah Jakarta dengan jenis dan kuantiti produk :</li><li>Untuk biaya pengiriman kami memiliki ketentuan yakni Rp. 10000 untuk wilayah Depok Tangerang & Bekasi dengan jenis dan kuantiti produk :</li></ul>','1',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('ZFN5ZlhRaHo1aFc3MEIyeGxuelRwdlcwdEN3ajhLbDQzbXpreGtKWjVTYz0','<p>Umah Multi berkomitmen untuk melindungi privasi pelanggannya. Kebijakan privasi ini berlaku untuk seluruh data pribadi pelanggan yang didapat secara online. Begitu juga data yang dikirimkan melalui form yang ada pada website kami dan aplikasi pihak ketiga seperti email. Data tersebut termasuk data yang dilindungi kami untuk menerapkan kebijakan privasi ini. Mohon untuk tidak mengirim data yang berpotensi menimbulkan permasalahan bagi pihak manapun.</p><p>Informasi yang kami terima, dapat kami gunakan untuk pengembangan kualitas pelayanan di masa mendatang seperti :</p><ul><li>Pengembangan User Xperience pelanggan dalam menggunakan website Umah Multi.</li><li>Untuk meningkatkan pelayanan dari feedback pelanggan.</li><li>Untuk mengelola keperluan dan konten website misal ulasan, promo, dll.</li></ul><h5>Apakah website Umah Multi aman untuk informasi pengunjung ?</h5><p>Pada website kami terdapat Secure Socket Layer (SSL), dimana data yang dikirimkan kepada server melalui jaringan terenkripsi dengan aman. Keamanan pada sistem website kami selalu di update secara berkala untuk menutup celah-celah yang ada.</p>','2',NULL,NULL,NULL,NULL,NULL,NULL,'Y');

/*Table structure for table `umh_m9l2t0_products` */

DROP TABLE IF EXISTS `umh_m9l2t0_products`;

CREATE TABLE `umh_m9l2t0_products` (
  `m9l2t0_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` varchar(200) NOT NULL,
  `product_name` varchar(50) DEFAULT NULL,
  `product_desc` text DEFAULT NULL,
  `product_desc_meta` varchar(200) DEFAULT NULL,
  `product_img` varchar(50) DEFAULT NULL,
  `product_img_alt` varchar(50) DEFAULT NULL,
  `product_cat` varchar(50) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `parent` varchar(200) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_product`),
  UNIQUE KEY `m9l2t0_product_id` (`m9l2t0_product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_products` */

insert  into `umh_m9l2t0_products`(`m9l2t0_product_id`,`id_product`,`product_name`,`product_desc`,`product_desc_meta`,`product_img`,`product_img_alt`,`product_cat`,`price`,`parent`,`created_by`,`created_date`,`created_ip`,`modified_by`,`modified_date`,`modified_ip`,`is_active`) values 
(1,'a1lGcE9oTDNVeGY0Qk14Q2hGOHdHQT09','Stempel','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'Q21xbFU3a1kzS2gzajhmTVlPMVFDZz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(2,'a1Q4aytWczBnNkc4anE5ZHNramdGUT09','Roll banner','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'c1pWck96bUtYbEozUi9KTG9xSUE3Zz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(3,'b3FLMXZ6YklyNHBZamp5TCtJMjlrQT09','Y banner','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'c1pWck96bUtYbEozUi9KTG9xSUE3Zz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(4,'bDdyd20raUljZEkyMXEzUHZNeUFBZz09','Memo','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'dzlVMUdWUHViVCs0b1NBZ0R5K3k1UT09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(5,'bEhRUFZSYk44QVFDNEpEamhCNjMvUT09','Spanduk','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'c1pWck96bUtYbEozUi9KTG9xSUE3Zz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(6,'c0MrRUw2TEU4UDgrK0dVRHhIOFQ0WEFSTmtEUlluU0RVR082SnBhSjRkUT0','Web Company Profile','Gratis Domain *,Gratis SSL,SEO Friendly,CMS Friendly,200 MB Disk',NULL,'web-compro.png','umah-multi','jasa-web',600000,'S0tPR2FjRk5wWjM1Y0h0UWxxNnNSRlp0eTlJdEw4REpGSVNCOUgvMFd1cz0',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(7,'dkEvOXgycURKL3VNREVUZlpNa1lsZz09','Nota Penjualan','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'dzlVMUdWUHViVCs0b1NBZ0R5K3k1UT09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(8,'eGo0UXNJYkc1NXB6VGh2Nm52aWg3dz09','Kartu Nama','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'KzVjaGVITnVGNlU1UkNUanVRemlYdz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(9,'eStmdUxTUHV0TWlvVTByMTZmZEJUdz09','Tas Belanja','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'N3g3SU43Y1hBWHFVZGVRVi9YejlkZz09',NULL,NULL,NULL,NULL,NULL,NULL,'N'),
(10,'K0dCcTZ5azBaSmt5RG9QQU5RUTh4QT09','Poster','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'c1pWck96bUtYbEozUi9KTG9xSUE3Zz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(11,'K0VBeUFzZUdNQVdCZmdLb1hkeFpKdz09','Pernikahan','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'Y2F6SU1hYTUwUS83Tm5YUElsVmh3UT09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(12,'L3ZRaXVXYVJCajhXR0p3QVFQdThoUT09','Majalah','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'Ty9UamlOYnVjMStobE5JcU83RjVrdz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(13,'LzFpWG4rbEJROE16b3BPeTluNnlHdFhSeEVKdlZyMVd3WDA3V3I5REEvVT0','Tugas Pendidikan','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'amRGUmtOQnZ2OGg3TlVJRHRoMkh6UT09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(14,'MmVyRDc0UkZ2KzdBY2ZqTnBDWnR0Zz09','Brosur','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'UG03SXVnOFhDVGVUdmFMaEc4QUJ4dz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(15,'Q1MyRUVQamZqbEhCdzdIQ05ycXJ6UT09','Kemasan Minuman','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'N3g3SU43Y1hBWHFVZGVRVi9YejlkZz09',NULL,NULL,NULL,NULL,NULL,NULL,'N'),
(16,'R0s2ZjRrVkJqRUs5MmR6YWJBL0crUT09','Flyer','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'UG03SXVnOFhDVGVUdmFMaEc4QUJ4dz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(17,'RjZXeS9xNHA5d3lXc21BYmNnSEptZz09','Stiker','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'N3g3SU43Y1hBWHFVZGVRVi9YejlkZz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(18,'T0FVTWxrdnMvQ2VzRHRYaEVCMVlZUT09','Tabloid','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'Ty9UamlOYnVjMStobE5JcU83RjVrdz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(19,'TGpPQldraWoyN0R4YmJWOXdwcVRJQT09','Web UMKM','Gratis Domain *,Gratis SSL,SEO Friendly,CMS Friendly,250 MB Disk',NULL,'web-umkm.png','umah-multi','jasa-web',500000,'S0tPR2FjRk5wWjM1Y0h0UWxxNnNSRlp0eTlJdEw4REpGSVNCOUgvMFd1cz0',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(20,'TXFqdW1ialoreFg4bytJaWtaa0VvUT09','Baliho','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'c1pWck96bUtYbEozUi9KTG9xSUE3Zz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(21,'UGduei9wRnBxNzBjYk5uVy9VN3M5QT09','Kemasan Makanan','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'N3g3SU43Y1hBWHFVZGVRVi9YejlkZz09',NULL,NULL,NULL,NULL,NULL,NULL,'N'),
(22,'UzhBdDk1dnhpYkRtUzRWUFl1S3Q5Zz09','Kalender','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'Q21xbFU3a1kzS2gzajhmTVlPMVFDZz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(23,'WEdUcENmcGFGOTh5b085SW1pQks4UT09','X banner','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'c1pWck96bUtYbEozUi9KTG9xSUE3Zz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(24,'WEl1OW5JNUQ0TjI3TTZYd2Rsd3ZsUT09','Pamflet','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'UG03SXVnOFhDVGVUdmFMaEc4QUJ4dz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(25,'WlJ0RVo2Mk9CQ29BNlF2QUM2V3NSTTFzTVYzblRJejZkcmdtcFM4dGJXST0','Sunatan / Khitanan','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'Y2F6SU1hYTUwUS83Tm5YUElsVmh3UT09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(26,'WlpMdDNoc3hid2tLT3o1UnpvalAzZz09','Desain Grafis',NULL,NULL,'stamp.jpg','umah-multi','jasa-web',8000,'S0tPR2FjRk5wWjM1Y0h0UWxxNnNSRlp0eTlJdEw4REpGSVNCOUgvMFd1cz0',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(27,'WnZBZHRsd0pvU2Nuc1RXbEpYSVV4QT09','Kartu RFID','Lorem ipsum dolor sit amet, consectetur adipisicing elit',NULL,'stamp.jpg','umah-multi','jasa-print-online',8000,'KzVjaGVITnVGNlU1UkNUanVRemlYdz09',NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
(28,'Y1FSVmhWUGRJUUNHZFJQWG9aWHVqUT09','Web Portfolio','Gratis Domain *,Gratis SSL,SEO Friendly,CMS Friendly,50 MB Disk',NULL,'web-portfolio.png','umah-multi','jasa-web',200000,'S0tPR2FjRk5wWjM1Y0h0UWxxNnNSRlp0eTlJdEw4REpGSVNCOUgvMFd1cz0',NULL,NULL,NULL,NULL,NULL,NULL,'Y');

/*Table structure for table `umh_m9l2t0_redeem_code` */

DROP TABLE IF EXISTS `umh_m9l2t0_redeem_code`;

CREATE TABLE `umh_m9l2t0_redeem_code` (
  `id_redeem` varchar(10) NOT NULL,
  `rc_name` varchar(50) DEFAULT NULL,
  `rc_code` varchar(10) DEFAULT NULL,
  `rc_desc` text DEFAULT NULL,
  `rc_start` datetime DEFAULT NULL,
  `rc_end` datetime DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_redeem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_redeem_code` */

/*Table structure for table `umh_m9l2t0_services` */

DROP TABLE IF EXISTS `umh_m9l2t0_services`;

CREATE TABLE `umh_m9l2t0_services` (
  `id_service` varchar(200) NOT NULL,
  `serv_img` varchar(50) DEFAULT NULL,
  `serv_img_alt` varchar(50) DEFAULT NULL,
  `serv_desc` varchar(200) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_service`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_services` */

insert  into `umh_m9l2t0_services`(`id_service`,`serv_img`,`serv_img_alt`,`serv_desc`,`created_by`,`created_date`,`created_ip`,`modified_by`,`modified_date`,`modified_ip`,`is_active`) values 
('eHA3allYZCsvdmtyTFlac3cyVFFrZz09','umahmulti.png','umahmulti-03',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('enRBa3BMS2ZkQjZRS1BwSE5PQW1QZz09','umahmulti.png','umahmulti-02',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y'),
('V09HSjdxTEd3MmVnMHNlWjkyUkxHQT09','umahmulti.png','umahmulti-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Y');

/*Table structure for table `umh_m9l2t0_size` */

DROP TABLE IF EXISTS `umh_m9l2t0_size`;

CREATE TABLE `umh_m9l2t0_size` (
  `id_size` varchar(10) NOT NULL,
  `size_name` varchar(50) DEFAULT NULL,
  `size_desc` text DEFAULT NULL,
  `product_id` varchar(10) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_size`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_size` */

/*Table structure for table `umh_m9l2t0_state` */

DROP TABLE IF EXISTS `umh_m9l2t0_state`;

CREATE TABLE `umh_m9l2t0_state` (
  `id_state` varchar(10) NOT NULL,
  `state_name` varchar(50) DEFAULT NULL,
  `state_desc` varchar(50) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_state`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_state` */

/*Table structure for table `umh_m9l2t0_tr_detail_prod` */

DROP TABLE IF EXISTS `umh_m9l2t0_tr_detail_prod`;

CREATE TABLE `umh_m9l2t0_tr_detail_prod` (
  `id_detail_prod` varchar(10) DEFAULT NULL,
  `id_tr_detail` varchar(10) DEFAULT NULL,
  `id_custom_field` varchar(10) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_tr_detail_prod` */

/*Table structure for table `umh_m9l2t0_transaksi` */

DROP TABLE IF EXISTS `umh_m9l2t0_transaksi`;

CREATE TABLE `umh_m9l2t0_transaksi` (
  `id_transaksi` varchar(10) NOT NULL,
  `cust_id` varchar(10) DEFAULT NULL,
  `is_design` int(1) DEFAULT NULL COMMENT '1: punya 2: tidak',
  `link_design` text DEFAULT NULL,
  `total_payment` decimal(10,0) DEFAULT NULL,
  `method_payment` int(5) DEFAULT NULL,
  `note_cust` varchar(250) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_transaksi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_transaksi` */

/*Table structure for table `umh_m9l2t0_transaksi_detail` */

DROP TABLE IF EXISTS `umh_m9l2t0_transaksi_detail`;

CREATE TABLE `umh_m9l2t0_transaksi_detail` (
  `id_tr_detail` varchar(10) NOT NULL,
  `id_transaksi` varchar(10) DEFAULT NULL,
  `product_id` varchar(10) DEFAULT NULL,
  `qty` int(10) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `sum` varchar(250) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_tr_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_transaksi_detail` */

/*Table structure for table `umh_m9l2t0_type` */

DROP TABLE IF EXISTS `umh_m9l2t0_type`;

CREATE TABLE `umh_m9l2t0_type` (
  `id_type` varchar(10) NOT NULL,
  `type_name` varchar(50) DEFAULT NULL,
  `type_desc` text DEFAULT NULL,
  `product_id` varchar(10) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_active` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_type` */

/*Table structure for table `umh_m9l2t0_users` */

DROP TABLE IF EXISTS `umh_m9l2t0_users`;

CREATE TABLE `umh_m9l2t0_users` (
  `id_user` varchar(10) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `passwd` varchar(200) DEFAULT NULL,
  `passwd_desc` varchar(200) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `img_profile` varchar(500) DEFAULT NULL,
  `level_id` varchar(10) DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_ip` varchar(16) DEFAULT NULL,
  `modified_by` varchar(10) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_ip` varchar(16) DEFAULT NULL,
  `is_login` char(1) DEFAULT NULL,
  `is_active` char(1) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `umh_m9l2t0_users` */

insert  into `umh_m9l2t0_users`(`id_user`,`username`,`passwd`,`passwd_desc`,`firstname`,`lastname`,`img_profile`,`level_id`,`created_by`,`created_date`,`created_ip`,`modified_by`,`modified_date`,`modified_ip`,`is_login`,`is_active`) values 
('WQPIpk0cY8','admin','fbade9e36a3f36d3d676c1b808451dd7','z','Administrator','',NULL,'vkhRbgiqyV',NULL,NULL,NULL,NULL,NULL,NULL,'N','Y');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
